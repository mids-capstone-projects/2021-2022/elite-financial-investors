# Elite Financial Investors


### Introdcution
There are numerous research showing that financial assets are important to be in the top 1% households (Piketty 2017; Piketty and Saez 2006). Another research article published in 2021 showed that business assets are critical for the top 1% households (Keister et al., 2021). The importance of financial assets and business assets has been thoroughly investigated, but real estate investments have not been. However, it is obscure whether this anecdotal understanding of how people get into the top wealthy club holds more broadly (Keister et al., 2021). This study expands the analysis logic of the original paper to real estate investments and investigates the role of real estate investments in the top 1% households.  

Using the data from the Survey of Consumer Finances (SCF data, 2021), the analysis focuses on analyzing the top 1% households and the role of real estate investments. SCF data is a triennial survey conducted to collect financial outcomes on U.S. households, such as income, investments, and demographics for the past 30 years (Codebook for SCF, 2021). The federal reserve has been publishing papers based on the SCF data, which focused on describing the overall U.S. households’ financial outcomes for a three-year period (Bhutta et al., 2020). The SCF data contains over 265,000 observations and over 35,000 observations in the top 1% net worth households in the past 30 years. For each year, there exist over 2,000 top 1% net worth household observations. The net worth of a household is the difference between their assets and debt.  

### Data 
The SCF is sponsored by the board of Governors of the Federal Reserve System with the cooperation of the Statistics of Income Division of the Internal Revenue Service (Codebook for SCF, 2021). It provides reliable and representative data on high-income and wealthy U.S households based on a nationwide survey conducted every 3 years.  

The raw dataset, also named Full Public Data Set, contains 7,521 variables including a household unique identifier, survey year, weight, and coded responses to the specific survey questions on three to seven thousand households each survey year, where the data has been altered systematically to ensure complete privacy and meanwhile can remain the results of data analyses unchanged (SCF data, 2021). 

In addition to the full dataset, the Summary Extract Public Data Set has 354 variables created from the raw dataset which has been transformed, cleaned, and inflated to the most recent 2019-dollar value (SCF Extract Data, 2021). For example, the net worth value is calculated from separate original variables on assets and liabilities and is only available from the extract data set. 

#### Datasets

Raw dataset: https://www.federalreserve.gov/econres/scfindex.htm

Extract dataset: https://sda.berkeley.edu/sdaweb/analysis/;jsessionid=AB46F0B3A20D5F5D72EBA88FA505C96F?dataset=scfcomb2019

Our final dataset: `final_0322.csv`

#### Codebooks

SCF Survey Codebook for Full dataset: https://www.federalreserve.gov/econres/files/codebk2019.txt

Interactive Codebook for Extract dataset: https://sda.berkeley.edu/sdaweb/analysis/?dataset=scfcomb2019

Our Codebook to record variables for data cleaning: https://docs.google.com/document/d/1rtm5YxzsiDrM16PGuSNUlmtvDKHFFTIzTjcw9BFObZI/edit#heading=h.hx32m9ar3664

### Analysis

- Part 1: How are top 1% households with and without real estate investment demographically different?
- Part 2: If top 1% households did not invest in real estate, what did they invest in? ​
- Part 3: How important is real estate investment to become the top 1%? ​

### Conclusion
The demographics of the two groups - top 1% households with real estate investment and without real estate investment – did not differ much with regards to marital status and race. Most of them were married and were white. Furthermore, the distribution of age and education had minimal differences. The top 1% net worth households with real estate investments were slightly denser in middle-age groups and lower in education level, compared to those without real estate investment.  

For those households in the top 1% and had no investment in real estate, their investment assets showed a significant difference in their education level. Households with master's or higher degrees invested significantly in financial assets whereas people with no bachelor's degree had more active business.  

To quantify the importance of real estate investment for the top 1% net worth households, necessity and sufficiency analyses were conducted. As a result, the top 1% households relied less on real estate investment to become the top 1% over the past 30 years, and fewer of them owned extremely expensive real estate investments. The results show that the role of real estate investments for U.S. households has become less critical in becoming the top 1% in net worth.  

This result aligns with past research which proved the increased significance of business assets (Keister et al., 2021). The results in this analysis confirm that the significance of real estate investments has decreased in the past thirty years to be in the one percent. Combining the results of past research and our study, it shows how the dynamics of investment assets changed over the past 30 years. Assuming that the trend will continue, investors should consider exploring investment opportunities in business assets and financial assets rather than real estate. 


### References  
Bricker, Jesse and Goodman, Sarena and Moore, Kevin B. and Henriques Volz, Alice and Ruh, Dalton, Wealth and Income Concentration in the SCF: 1989–2019 (September 28, 2020). FEDS Notes No. 2020-09-28-1, Available at SSRN: https://ssrn.com/abstract=3703686 or http://dx.doi.org/10.17016/2380-7172.2795 

Bricker, Jesse, et al. “Updates to the Sampling of Wealthy Families in the Survey of Consumer Finances.” Finance and Economics Discussion Series, vol. 2017, no. 114, 2017, https://doi.org/10.17016/feds.2017.114. 

Bricker, Jesse, et al. “Wealth Concentration in the U.S. after Augmenting the Upper Tail of the Survey of Consumer Finances.” Economics Letters, vol. 184, 2019, p. 108659., https://doi.org/10.1016/j.econlet.2019.108659. 

Neil Bhutta et al., “Changes in U.S. Family Finances from 2016 to 2019: Evidence from the Survey of Consumer Finances”. Federal Reserve Board. September 2020. https://www.federalreserve.gov/publications/files/scf20.pdf.   

Juliana Menasce Horowitz et al., “Most Americans say there is too much economic inequality in the U.S., but fewer than half call it a top priority”. Pew Research Center. https://www.pewresearch.org/social-trends/2020/01/09/trends-in-income-and-wealth-inequality/, Jan 9th, 2020. 

Lisa A. Keister et al., “Do You Need Business Assets to Be Rich?” Socius: Sociological Research for a Dynamic World Volume 7: 1–3. July 2021.  

Hanna, Sherman D., et al. “Behind the Numbers: Understanding the Survey of Consumer Finances.” Journal of Financial Counseling and Planning, vol. 29, no. 2, 2018, pp. 410–418., https://doi.org/10.1891/1052-3073.29.2.410. 

Kennickell, Arthur B. “Multiple Imputation in the Survey of Consumer Finances.” Statistical Journal of the IAOS, vol. 33, no. 1, 2017, pp. 143–151., https://doi.org/10.3233/sji-160278. 

Kennickell, Arthur B., and R. Louise Woodburn. “Consistent Weight Design for the 1989, 1992 and 1995 SCFS, and the Distribution of Wealth.” Review of Income and Wealth, vol. 45, no. 2, 1999, pp. 193–215., https://doi.org/10.1111/j.1475-4991.1999.tb00328.x. 

Survey of Consumer Finances - Extract File 1989-2013, University of California, Berkeley, https://sda.berkeley.edu/Abstracts/SCF.html. 

Suzanne Lindamood et al. “Using the Survey of Consumer Finances: Some Methodological Considerations and Issues.” Journal of Consumer Affairs, vol. 41, no. 2, 2007, pp. 195–222., https://doi.org/10.1111/j.1745-6606.2007.00075.x. 
