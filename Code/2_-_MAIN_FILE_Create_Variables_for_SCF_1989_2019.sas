/*2 - MAIN FILE Create Variables for SCF 1989_2019.SAS

*****THIS IS THE MAIN FILE FOR CREATING VARIABLES FOR ALL PROJECTS USING THE 1989-2019 SCF*******

Input data: 	1) Combinedextract (all SCF years, extracted data with Fed-created variable names) - THESE ARE INFLATED TO CURRENT DOLLARS
					ALWAYS DOWNLOAD NEW VERSIONS TO HAVE EVERYTHING CURRENT AND IN CURRENT YEAR DOLLARS

				2) Combinedfull  (all SCF years, full data sets with select variables) THESE ARE NOT INFLATED!!!!!!!!!!!!

				**input data were created in 1 - Prep Data.SAS
 
Tasks:			Create variables for analysis

Output data:	SCFforAnaylsis.sas7bdat
*/

 
/*Part 1. Create variables using extract data*/

/*libname SCF 		'C:\Lisa\SAS\SCF 1989-2016\Data\SCF Extract Data Files';	
/*libname SCFFULL 	'C:\Lisa\SAS\SCF 1989-2016\Data\SCF Full Data Files';

/*$$$$$$$$$$$$   Update libnames IN DATA STEPS when 2019 data are available   
					I ALREADY USE THESE AT THE END TO SAVE NEW VERSIONS OF THE DATA WTIH VALUES INFLATED TO 2019$$$$$$$*/
libname SCF2		'C:\Lisa\SAS\SCF 1989-2019\Data\SCF Extract Data Files';
libname SCFFULL2	'C:\Lisa\SAS\SCF 1989-2019\Data\SCF Full Data Files';

Data temp1;
	Set SCF2.SCFEXTR; 

/*Education - new code as of November 2017. this uses the SCF extract variable "educ"
	that was revised in November 2017 to reflect the 2016 survey. all prior years
	were recoded to match the 2016 categories*/

	if educ le 7 then LessThanHS = 1;
		else if educ = . then LessThanHS = .;
		else LessThanHS = 0;

	if educ = 8 then HSGraduate = 1;
		else if educ = . then HSGraduate = .;
		else HSGraduate = 0;

	if educ = 9 or educ = 10 or educ = 11 then SomeCollege = 1;
		else if educ = . then SomeCollege = .;
		else SomeCollege = 0;
	
	if educ = 12 then Bachelors = 1;
		else if educ = . then Bachelors = .;
		else Bachelors = 0;

	if educ = 13 or educ = 14 then AdvancedDegree = 1;
		else if educ = . then AdvancedDegree = .;
		else AdvancedDegree = 0;

/*Continuous education variable*/
	REducYears = edcl;

/*Race*/
		if race=1 then White=1;
			else White=0;
		if race=2 then Black=1;
			else Black=0;
		if race=3 then Latino=1;
			else Latino=0;
		if race=5 then Other=1;
			else Other=0;

	if latino = 1 then race_3 = 1;
		else if black = 1 then race_3 = 2; 
		else if white = 1 then race_3 = 3; 


/*Lifecycle of household head
		1 = head under 55 + not married/LWP + no children
		2 = head under 55 + married/LWP + no children
		3 = head under 55 + married/LWP + children
		4 = head under 55 + not married/LWP + children
		5 = head 55 or older and working
		6 = head 55 or older and not working*/

	Lifecycle = lifecl;


/*Age squared; age centered and divided by ten -- see below for more detailed variables*/
		age_sq = age*age;

		age_c = (age - 50.7)/10; 

		age_c2 = age_c**2;


/*Age Categories*/
		if age>24 & age<65 then Age25to64=1;
			else Age25to64=0;

		if age=>25 & age<40 then Age25to39=1; else Age25to39=0;
		if age=>40 & age<50 then Age40to49=1; else Age40to49=0;
		if age=>50 & age<60 then Age50to59=1; else Age50to59=0;
		if age=>60 & age<70 then Age60to69=1; else Age60to69=0;
		if age=>70 then Age70P=1; else Age70P=0;


		if age=<29 then AgeCategories1 = '20s'; 
			else if age=>30 & age=<39 then AgeCategories1 = '30s'; 
			else if age=>40 & age=<49 then AgeCategories1 = '40s'; 
			else if age=>50 & age=<59 then AgeCategories1 = '50s'; 
			else if age=>60 & age=<69 then AgeCategories1 = '60s'; 
			else if age=>70 then AgeCategories1 = '70plus'; 
			else AgeCategories1 = 0; 


		if age=<29 then AgeCategories2 = 1; 
			else if age=>30 & age=<39 then AgeCategories2 = 2; 
			else if age=>40 & age=<49 then AgeCategories2 = 3; 
			else if age=>50 & age=<59 then AgeCategories2 = 4; 
			else if age=>60 & age=<69 then AgeCategories2 = 5; 
			else if age=>70 then AgeCategories2 = 6; 
			else AgeCategories2 = 0; 

		if age=<29 				then AgeTwenties  = 1; else AgeTwenties  = 0;
		if age=>30 & age=<39 	then AgeThirties  = 1; else AgeThirties  = 0;
		if age=>40 & age=<49 	then AgeForties   = 1; else AgeForties   = 0;
		if age=>50 & age=<59 	then AgeFifties   = 1; else AgeFifties   = 0;
		if age=>60 & age=<69 	then AgeSixties   = 1; else AgeSixties   = 0;
		if age=>70 				then AgeSeventies = 1; else AgeSeventies = 0;

/*Birth year*/
		BirthYear = year - age; 

/*Generations - Broad Categories*/
		if BirthYear ge 1982 and BirthYear le 2004 then generation_Y 	= 1; else generation_Y  = 0;
		if BirthYear ge 1965 and BirthYear le 1981 then generation_X	= 1; else generation_X 	= 0;
		if BirthYear ge 1946 and BirthYear le 1964 then generation_bb	= 1; else generation_bb = 0;
		if BirthYear le 1945 					   then generation_gg	= 1; else generation_gg = 0;

/*Two baby boom groups*/
		if BirthYear ge 1946 and BirthYear le 1954 then generation_bb_old	= 1; else generation_bb_old = 0;
		if BirthYear ge 1955 and BirthYear le 1964 then generation_bb_yng	= 1; else generation_bb_yng = 0;

/*Married*/
		if Married=2 then Married=0;

/*Children*/
		if kids=0 then have_kids = 0;
			else have_kids = 1;
		num_kids = kids; 

/*Household has no checking account*/
		no_checking = nochk;


/*Work status -- see below for more detailed variables*/
		if occat1 = 1 then work_others = 1;
			else work_others = 0;

		if occat1 = 2 then work_self = 1;
			else work_self = 0;

		if occat1 = 3 then work_retired = 1;
			else work_retired = 0;

		if occat1 = 4 then work_unemployed = 1;
			else work_unemployed = 0;


/*Occupation -- see below for more detailed variables*/
		if occat2 = 1 then occup_professional = 1;
			else occup_professional = 0;

		if occat2 = 2 then occup_technical = 1;
			else occup_technical = 0;

		if occat2 = 3 then occup_labor = 1;
			else occup_labor = 0;

		if occat2 = 4 then occup_NotWorking = 1;
			else occup_NotWorking = 0;


/*****************************************************/
/*Wealth Variables*/
		Assets=Networth+Debt;

/*Primary Residence: ownership, value, debt, and equity*/
		HomeOwner	= hhouses;

		HomeValue	= houses;
			if HomeValue = -1 then HomeValue = 0;


		HomeDebt	= mrthel;

		HomeEquity	= homeeq;
			if HomeEquity = -1 then HomeEquity = 0;

		/*Home Owner defined to include farms, too*/
		if homeeq~=0 then HomeOwner1=1;
			else if homeeq=. | homeeq=0 then Homeowner1=0;
		if housecl~=2 then HomeOwner1=1;
			else Homeowner1=0;

		/*This is redundant.  Models of mortgages should exclude non-home owners*/
		if HomeOwner1=1 & PAYMORT1=0 then Mortgages=0;
			else if Homeowner1=1 & PAYMORT1>0 & PAYMORT2>0 & PAYMORT3>0 then Mortgages=3 ;
			else if Homeowner1=1 & PAYMORT1>0 & PAYMORT2>0 & PAYMORT3=0 then Mortgages=2 ;
			else if Homeowner1=1 & PAYMORT1>0 & PAYMORT2=0 & PAYMORT3=0 then Mortgages=1 ;
			else Mortgages=0; 
		
/*OTHER Residential Real Estate: ownership, value, debt, and equity*/
		if oresre=0 then OtherResOwner = 0;
			else OtherResOwner = 1;

		OtherResValue	= Oresre;
			if OtherResValue = -1 then OtherResValue = 0;

		OtherResDebt	= Resdbt;

		OtherResEquity	= Oresre - Resdbt;
			if OtherResEquity = -1 then OtherResEquity = 0;


/*NON Residential Real Estate: 	ownership and equity only....
								separate information about value and debt are not in the extract data; 
								we can pull those details from the full data if necessary*/

		if nnresre=0 then NonResOwner = 0;
			else NonResOwner = 1;
		
		NonResEquity = nnresre;


/*ANY Real Estate: ownership only....adding equity across residential and non-residential doesn't make sense; and we only have equity
			for non-residential*/
		if HomeOwner = 1 or OtherResOwner = 1 or NonResOwner = 1 then AnyRealOwner = 1;
			else AnyRealOwner = 0;

/*Liquid Asset Rates of Ownership*/ 
		if hstocks=1 | deq>0 then StockMutual=1;
			else StockMutual=0;
		if mma>0 then InterestBank=1;
			else InterestBank=0;
		if nochk=0 then Checking1=1;
			else Checking1=0;
		If irakh>0 then IRAor401k=1;
			else  IRAor401k=0;

/*Liquid Asset and Debt values*/
		Financial			= fin; /*total financial assets*/
		CheckingValue 		= checking;
		SavingsPlanValue 	= thrift;
		StockValue 			= deq;
		IraValue 			= irakh;
		LifeInsuranceValue 	= cashli;
		OtherAssetValue 	= othnfin;
		SecuredDebtValue 	= mrthel;
		CreditCardDebtValue = ccbal;
		BusinessValue 		= bus;

/*Other Variables*/
		Female				= hhsex - 1;
		Unemployed 			= 1 - lf;

		if OCCAT2=1 then Manager=1;
		else Manager=0;

/*Cars*/
		CarDebt 			= VEH_INST;
		VehicleValue 		= vehic;
		OwnCar 				= own;
		NumberCarOwned 		= nown;
		if NumberCarOwned>5 then NumberCarOwned=5;


/*Log some key asset values*/

LoggedHomeValue  			= log(HomeValue+1);
LoggedCheckingValue			= log(CheckingValue+1); 		
LoggedStockValue 			= log(StockValue+1);			
LoggedBusinessValue			= log(BusinessValue+8000000);
LoggedHomeDebt				= log(HomeDebt+1);
LoggedCCardDebtValue		= log(CreditCardDebtValue+1);


/*Year is coded as character in some years and numeric in other years; this code standardizes that*/
		yeartemp=year*1;
			drop year;
			rename yeartemp=year;


/*I don't use this code because I created person id in the previous program
		if year<1992 then Y1temp= X1*1; else Y1temp=Y1*1;
		if year<1992 then YY1temp= XX1*1; else YY1temp=YY1*1;
		drop Y1;
		drop YY1;
		rename Y1temp=Y1 ;
		rename YY1temp=YY1; 
*/

			
/*Percent Assets in Stocks*/
		PAssetsInStocks=(StockValue) /(Assets+.01);
		if PAssetsInStocks=. then PAssetsInStocks=0;
		if PAssetsInStocks<0 then PAssetsInStocks=0;
		if PAssetsInStocks>1 then PAssetsInStocks=1;

/*Percent Assets in Financial assets*/
		PAssetsInFinance=fin/(Assets+.01);
		if PAssetsInFinance=. then PAssetsInFinance=0;
		if PAssetsInFinance<0 then PAssetsInFinance=0;
		if PAssetsInFinance>1 then PAssetsInFinance=1;

/*Percent Assets in Residential investment real estate (NOT primary residence)*/
		PAssetsInOtherResReal=OtherResEquity/(Assets+.01);
		if PAssetsInOtherResReal=. then PAssetsInOtherResReal=0;
		if PAssetsInOtherResReal<0 then PAssetsInOtherResReal=0;
		if PAssetsInOtherResReal>1 then PAssetsInOtherResReal=1;

/*Percent Assets in Non-Residential real estate */
		PAssetsInNonResReal=NonResEquity/(Assets+.01);
		if PAssetsInNonResReal=. then PAssetsInNonResReal=0;
		if PAssetsInNonResReal<0 then PAssetsInNonResReal=0;
		if PAssetsInNonResReal>1 then PAssetsInNonResReal=1;



/*Income sources*/
		CapGains_Unrealized		= KGSTMF;				/*Unrealized capital gains or losses on stocks and mutual funds*/
		CapGains_Realized		= KGINC;				/*Capital gain or loss income*/
		Salary					= WAGEINC;				/*Wage and salary income*/
			if salary lt 0 then salary = 0; 			/*this fixes one case with negative salary*/
		Dividends				= INTDIVINC;
		Bus_FarmIncome			= BUSSEFARMINC; 		/*Income from business, sole proprietorship, and farm   X5704+X5714*/

		PIncomeCapitalGains		= CapGains_Realized/(Income+.01);
		PIncomeDividends		= Dividends/(Income+.01);
		PIncomeSalary			= Salary/(Income+.01);
		PIncomeBusiness			= Bus_FarmIncome/(Income+.01);


		Normal_income			= Norminc;



/*log income and net worth; we don't ultimately use the logged net worth because adding such a huge
		negative value ends up creating some odd outcomes*/
loggednetworth  = log(networth+300000000);   

loggedincome	= log(income+1); 


/*cubed net worth ...this is SAS's way of fixing an error in calculations of negative cube roots*/
cubednetworth=SIGN(networth)*ABS(networth)**(1/3);

/*Divide net worth by 1,000*/
networth000 = networth/1000;


/*logged variables -- this was my original way of logging variables; I stopped doing it
  because it handles negatives in an unusual way. we opted instead to use the code above
	Array SkewedVariable (17) income networth debt assets homeequity CheckingValue SavingsPlanValue 
			StockValue IraValue LifeInsuranceValue OtherRealEstateValue BusinessValue OtherAssetValue 
			SecuredDebtValue HomeDebt CreditCardDebtValue CarDebt;
	Array LoggedVariable (17) loggedincome loggednetworth loggeddebt loggedassets loggedhomeequity 
			loggedCheckingValue loggedSavingsPlanValue loggedStockValue loggedIraValue loggedLifeInsuranceValue 
			loggedOtherRealEstateValue loggedBusinessValue loggedOtherAssetValue loggedSecuredDebtValue loggedHomeDebt 
			loggedCreditCardDebtValue loggedCarDebt;
	Do i = 1 to 16;
		if SkewedVariable(i)>0 then LoggedVariable(i)=log10(SkewedVariable(i)); 	  /*This without if statement would produce an 
																				        error and missing number for those with negative 
																				        and 0 networth.
		if SkewedVariable(i)<0 then LoggedVariable(i)=-log10(ABS(SkewedVariable(i))); /*This gives me negative logged networth
		if SkewedVariable(i)=0 then LoggedVariable(i)=0; 							  /*This is not actually true, but log of 1=0, and 0 is pretty close to 1...
	END;


*Leisure goods;
	*HomeOwner - Home Owner
	*HomeEquity - Money in Primary House
	*Houses - Value of Primary Residence
	*Oresre - Value of Other Residence
	*NNResre - Value in nonresidential real estate
	*VLease - Value of Leased Cars
	*Nown - Number of owned Vehicles
	*NLease - Number of Leased Vehicles
	*NewCar1 - Number of cars less than two years old
	*NewCar2 - Number of cars less than one year old
	*FoodHome
	*FoodDelv
	*FoodAway;
	*Characters number;


/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
/*Drop a bunch of variables*/


*retain necessary variables...these variables are in the middle of the sequence we want to drop..rename them twice to retain them;

/*	incometemp=income;
	agetemp=age;
	networthtemp=networth;
	drop income age networth;

	rename incometemp=income;
	rename agetemp=age;
	rename networthtemp=networth;
*Drop unneeded variables;
	drop agecl--dataset;
run;
*/


/*****************************************************/
/*Part 2. Create variables using FULL data*/

/**&&&&$$$$      REMEMBER TO INFLATE ANY VALUES FROM THE FULL DATA!!!!!!!!********************/


Data temp2;
	set SCFFULL2.SCFFULL;


/*Original respondent and spouse/partner variables reversed.
	All heterosexual couples in the SCF have the male adult as the head of household. 
	All married, same sex couples have the older adult as the head of household.
	this variables indicates the respondent and spouse were reversed to maintain this convention. 
                       
                       THE PERSON REFERRED TO AS THE "HEAD" IN THIS CODEBOOK IS
                       EITHER THE MALE IN A MIXED-SEX COUPLE OR THE OLDER
                       INDIVIDUAL IN A SAME-SEX COUPLE.  WHERE X8000=1, ALL
                       VARIABLES IN THE DATA SET THAT ORIGINALLY REFERRED TO
                       "RESPONDENT" AND "SPOUSE/PARTNER" AND ALL CODES THAT
                       CONTAIN THE SAME REFERENCES HAVE BEEN REVERSED.  
*/


RAge = X8022; if RAge lt 17 then RAge = X14; /*adjust 30 cases who corrected their age during the interivew*/
Spouse_age = X104; /*we leave spouse_age = 0 for those with no spouse because models will include married*spouse_age*/
Spouse_age_sq = X104*X104;
Age_gap = RAge-spouse_age; /*if Spouse_age = . then Age_gap = .;*/

if Spouse_age gt (RAge + 2) then Wife_older = 1;
	else Wife_older = 0;
if Spouse_age lt (RAge - 2) then Husband_older = 1;
	else Husband_older = 0;
if wife_older ne 1 and Husband_older ne 1 then Same_age = 1;
	else Same_age = 0;

/*Old and Young age groups*/
	if RAge 		ge 55 then R_old = 1; 	else R_old = 0;
	if RAge 		lt 55 then R_yng = 1; 	else R_yng = 0;

	if Spouse_age	ge 55 then S_old = 1; 	else S_old = 0;
	if Spouse_age	lt 55 then S_yng = 1; 	else S_yng = 0;



/*Education; spouse education; educational homogamy
	I need to return to the full dataset to do this because spouse education is not in the extract data.
	I use two sets of variables to create this because the SCF changed the variable number in 2016 for education.
	1989-2013	respondent education = X5901 and spouse education = X6101
	2016		respondent education = X5931 and spouse education = X6111*/

	/*respondent*/ 
	if X5901 = -1 then X5901 = 1;	/*change -1 to 1 so that people with no education are included with those who have LessThanHS*/
	if X5931 = -1 then X5931 = 1;

	R_LessThanHS = 0;
		if X5931 ge 1 and X5931 le 7 then R_LessThanHS = 1;
		if (X5901 ge 0 and X5901 le 12) and X5902 = 5  then R_LessThanHS = 1;	/*X5901 is years of educ; X5902 indicates graduated hs*/
		
	if X5931 = 8 then R_HSGraduate = 1;
		else if X5901 = 12 and X5902 = 1 then R_HSGraduate = 1;
		else R_HSGraduate = 0;

	if X5931 = 9 or X5931 = 10 or X5931 = 11 then R_SomeCollege = 1;
		else if X5901 gt 12 and X5901 lt 16 then R_SomeCollege = 1;
		else R_SomeCollege = 0;
	
	if X5931 = 12 then R_Bachelors = 1;
		else if X5901 = 16 then R_Bachelors = 1;
		else R_Bachelors = 0;

	if X5931 ge 13 then R_AdvancedDegree = 1;
		else if X5901 gt 16 then R_AdvancedDegree = 1;
		else R_AdvancedDegree = 0;

	/*spouse*/
	if X6101 = -1 then X6101 = 1;	/*change -1 to 1 so that people with no education are included with those who have LessThanHS*/
	if X6111 = -1 then X6111 = 1;

	SP_LessThanHS = 0;
		if X6111 ge 1 and X6111 le 7 then SP_LessThanHS = 1;
		if (X6101 ge 0 and X6101 le 12) and X6102 = 5  then SP_LessThanHS = 1;	/*X6101 is years of educ; X6102 indicates graduated hs*/

	if X6111 = 8 then SP_HSGraduate = 1;
		else if X6101 = 12 and X6102 = 1 then SP_HSGraduate = 1;
		else SP_HSGraduate = 0;

	if X6111 = 9 or X6111 = 10 or X6111 = 11 then SP_SomeCollege = 1;
		else if X6101 gt 12 and X6101 lt 16 then SP_SomeCollege = 1;
		else SP_SomeCollege = 0;
	
	if X6111 = 12 then SP_Bachelors = 1;
		else if X6101 =16 then SP_Bachelors = 1;
		else SP_Bachelors = 0;

	if X6111 ge 13 then SP_AdvancedDegree = 1;
		else if X6101 gt 16 then SP_AdvancedDegree = 1;
		else SP_AdvancedDegree = 0;


	/*educational homogamy*/
	if R_LessThanHS = 1 and SP_LessThanHS = 1 then Both_LessThanHS = 1;
		else Both_LessThanHS = 0;

	if R_HSGraduate = 1 and SP_HSGraduate = 1 then Both_HSGraduate = 1;
		else Both_HSGraduate = 0;

	if R_SomeCollege = 1 and SP_SomeCollege = 1 then Both_SomeCollege = 1;
		else Both_SomeCollege = 0;

	if R_Bachelors = 1 and SP_Bachelors = 1 then Both_Bachelors = 1;
		else Both_Bachelors = 0;

	if R_AdvancedDegree = 1 and SP_AdvancedDegree = 1 then Both_AdvancedDegree = 1;
		else Both_AdvancedDegree = 0;

	if (X5931 gt X6111) or (X5901 gt X6101) then Husband_MoreEduc = 1;
		else Husband_MoreEduc = 0;

	if (X6111 gt X5931) or (X6101 gt X5901) then Wife_MoreEduc = 1;
		else Wife_MoreEduc = 0;

	SameEduc=0;
	if Both_LessThanHS		=1 then SameEduc=1;
	if Both_HSGraduate		=1 then SameEduc=1;
	if Both_SomeCollege		=1 then SameEduc=1;
	if Both_Bachelors		=1 then SameEduc=1;
	if Both_AdvancedDegree	=1 then SameEduc=1;

	REd_FiveCat=.;
		if X5931 > 0 & X5931 < 8 				then REd_FiveCat = 1;
		if X5901 >= 0 & X5901 < 13 & X5902=5 	then REd_FiveCat = 1;
		if X5931 = 8 							then REd_FiveCat = 2;
		if X5901 = 12 and X5902 = 1 			then REd_FiveCat = 2;
		if X5931 = 9 | X5931 = 10 | X5931 = 11 	then REd_FiveCat = 3;
		if X5901 > 12 & X5901 < 16 				then REd_FiveCat = 3;
		if X5931 = 12 							then REd_FiveCat = 4;
		if X5901 = 16 							then REd_FiveCat = 4;
		if X5931 > 12 							then REd_FiveCat = 5;
		if X5901 > 16 							then REd_FiveCat = 5;
	SEd_FiveCat=.;
		if X6111 > 0 & X6111 < 8 				then SEd_FiveCat = 1;
		if X6101 >= 0 & X6101 < 13 & X6102=5 	then SEd_FiveCat = 1;
		if X6111 = 8 							then SEd_FiveCat = 2;
		if X6101 = 12 and X6102 = 1 			then SEd_FiveCat = 2;
		if X6111 = 9 | X6111 = 10 | X6111 = 11 	then SEd_FiveCat = 3;
		if X6101 > 12 & X6101 < 16 				then SEd_FiveCat = 3;
		if X6111 = 12 							then SEd_FiveCat = 4;
		if X6101 = 16 							then SEd_FiveCat = 4;
		if X6111 > 12 							then SEd_FiveCat = 5;
		if X6101 > 16 							then SEd_FiveCat = 5;

	Ed_gap = REd_FiveCat - SEd_FiveCat;



/*Detailed occupation; this is as much detail as the SCF includes in the public data*/
R_occup = X7402; 	LABEL R_occup = "Seven category occupation, Respondent"; 
S_occup = X7412; 	LABEL S_occup = "Seven category occupation, Spouse";

if R_occup = 1 then R_occup_agric = 1;
	else R_occup_agric = 0;
	LABEL R_occup_agric = "agriculture, veterninary services, landscaping, etc.";

if R_occup = 2 then R_occup_mining = 1;
	else R_occup_mining = 0;
	LABEL R_occup_mining = "mining, construction, etc.";

if R_occup = 3 then R_occup_manuf = 1;
	else R_occup_manuf = 0;
	LABEL R_occup_manuf = "manufacturing, publishing, artists, etc.";

if R_occup = 4 then R_occup_retail = 1;
	else R_occup_retail = 0;
	LABEL R_occup_retail = "retail, wholesale, restaurants, etc.";

if R_occup = 5 then R_occup_FIRE = 1;
	else R_occup_FIRE = 0;
	LABEL R_occup_FIRE = "finance, insurance, real estate, employment services, research, etc.";

if R_occup = 6 then R_occup_tech = 1;
	else R_occup_tech = 0;
	LABEL R_occup_tech = "technical services, legal, accounting, utilities, transportation, etc";

if R_occup = 7 then R_occup_public = 1;
	else R_occup_public = 0;
	LABEL R_occup_public = "public service, etc.";


if S_occup = 1 then S_occup_agric = 1;
	else S_occup_agric = 0;
	LABEL S_occup_agric = "agriculture, veterninary services, landscaping, etc.";

if S_occup = 2 then S_occup_mining = 1;
	else S_occup_mining = 0;
	LABEL S_occup_mining = "mining, construction, etc.";

if S_occup = 3 then S_occup_manuf = 1;
	else S_occup_manuf = 0;
	LABEL S_occup_manuf = "manufacturing, publishing, arti, etc.";

if S_occup = 4 then S_occup_retail = 1;
	else S_occup_retail = 0;
	LABEL S_occup_retail = "retail, wholesale, restaurants, etc.";

if S_occup = 5 then S_occup_FIRE = 1;
	else S_occup_FIRE = 0;
	LABEL S_occup_FIRE = "finance, insurance, real estate, employment services, research, etc.";

if S_occup = 6 then S_occup_tech = 1;
	else S_occup_tech = 0;
	LABEL S_occup_tech = "technical services, legal, accounting, utilities, transportation, etc";

if S_occup = 7 then S_occup_public = 1;
	else S_occup_public = 0;
	LABEL S_occup_public = "public service, etc.";


if R_occup_FIRE = 1 and S_occup_FIRE = 1 then Both_occup_FIRE = 1;
	else Both_occup_FIRE = 0;
	LABEL Both_occup_FIRE = "Both respondent and spouse are in FIRE occupations";



if X8000 = 1 then head_reversed = 1;
	else head_reversed = 0; 


/*Marital status*/
if X8023 = 1 or X8023 = 2 then married2 = 1;	/*Data check: married/living together; should be identical to married variable in extract data, above*/
	else married2 = 0;

if X8023 = 3 or X8023 = 4 then divorced = 1;
	else divorced = 0;

if X8023 = 5 then widowed = 1;
	else widowed = 0;

if X8023 = 6 then nevermarried = 1;
	else nevermarried = 0;


/*code gender using full sample; i need to do this to identify same sex couples*/
if X8021 = 2 then female2 = 1; 
	else female2 = 0;

if X8021 = 2 and X103 = 2 then married_women = 1;
	else married_women = 0;

if X8021 = 1 and X103 = 1 then married_men = 1;
	else married_men = 0;


/*Parents' Education **************ONLY AVAILABLE IN 2016+*/
if X6032 = -1 then X6032 = 1;
if X6132 = -1 then X6132 = 1;
if X6033 = -1 then X6033 = 1;
if X6133 = -1 then X6133 = 1;

RMother_LTHS = 0;
	if X6032 ge 1 and X6032 le 7 then RMother_LTHS = 1;

RMother_HS = 0;
	if X6032 = 8 then RMother_HS = 1;

RMother_SC = 0;
	if X6032 ge 9 and X6032 le 11 then RMother_SC = 1;

RMother_BA = 0;
	if X6032 = 12 then RMother_BA = 1;

RMother_AD = 0;
	if X6032 ge 13 then RMother_AD = 1;


RFather_LTHS = 0;
	if X6033 ge 1 and X6033 le 7 then RFather_LTHS = 1;

RFather_HS = 0;
	if X6033 = 8 then RFather_HS = 1;

RFather_SC = 0;
	if X6033 ge 9 and X6033 le 11 then RFather_SC = 1;

RFather_BA = 0;
	if X6033 = 12 then RFather_BA = 1;

RFather_AD = 0;
	if X6033 ge 13 then RFather_AD = 1;


SMother_LTHS = 0;
	if X6132 ge 1 and X6132 le 7 then SMother_LTHS = 1;

SMother_HS = 0;
	if X6132 = 8 then SMother_HS = 1;

SMother_SC = 0;
	if X6132 ge 9 and X6132 le 11 then SMother_SC = 1;

SMother_BA = 0;
	if X6132 = 12 then SMother_BA = 1;

SMother_AD = 0;
	if X6132 ge 13 then SMother_AD = 1;


SFather_LTHS = 0;
	if X6133 ge 1 and X6133 le 7 then SFather_LTHS = 1;

SFather_HS = 0;
	if X6133 = 8 then SFather_HS = 1;

SFather_SC = 0;
	if X6133 ge 9 and X6133 le 11 then SFather_SC = 1;

SFather_BA = 0;
	if X6133 = 12 then SFather_BA = 1;

SFather_AD = 0;
	if X6133 ge 13 then SFather_AD = 1;


/*add more detail if necessary later....this is the codebook
X6032(#1)       What is the highest level of school or the highest degree MOTHER
X6132(#2)       completed?
                RECORD THE HIGHEST LEVEL OF EDUCATION COMPLETED, NOT THE
                TIME IT TOOK TO COMPLETE IT.  DO NOT INCLUDE TRADE SCHOOLS
                AS COLLEGE.

                     1.    *1st, 2nd, 3rd, or 4th grade
                     2.    *5th or 6th grade
                     3.    *7th and 8th grade
                     4.    *9th grade
                     5.    *10th grade
                     6.    *11th grade
                     7.    *12th grade, no diploma
                     8.    *High school graduate - high school diploma or equivalent
                     9.    *Some college but no degree
                    10.    *Associate degree in college - occupation/vocation program
                    11.    *Associate degree in college - academic program
                    12.    *Bachelor's degree (for example: BA, AB, BS)
                    13.    *Master's degree ( for exmaple: MA, MS, MENG, MED, MSW, MBA)
                    14.    *Professional school degree (for example: MD, DDS, DVM, LLB, JD)
                    15.    *Doctorate degree (for example: PHD, EDD)
                    -1.    *Less than 1st grade
                     0.     Inap. (no spouse/partner;)
                *********************************************************
                    FOR THE PUBLIC DATA SET, CODES 2, 3, 4, 5, 6, AND 7
                    ARE COMBINED WITH CODE 1; CODE 10 AND CODE 11 ARE
                    COMBINED WITH CODE 9, AND; CODES 13, 14, AND 15 ARE
                    COMBINED WITH CODE 12
                *********************************************************

	
X6033(#1)       What is the highest level of school or the highest degree FATHER
X6133(#2)       completed?
                RECORD THE HIGHEST LEVEL OF EDUCATION COMPLETED, NOT THE
                TIME IT TOOK TO COMPLETE IT.  DO NOT INCLUDE TRADE SCHOOLS
                AS COLLEGE.

                     1.    *1st, 2nd, 3rd, or 4th grade
                     2.    *5th or 6th grade
                     3.    *7th and 8th grade
                     4.    *9th grade
                     5.    *10th grade
                     6.    *11th grade
                     7.    *12th grade, no diploma
                     8.    *High school graduate - high school diploma or equivalent
                     9.    *Some college but no degree
                    10.    *Associate degree in college - occupation/vocation program
                    11.    *Associate degree in college - academic program
                    12.    *Bachelor's degree (for example: BA, AB, BS)
                    13.    *Master's degree ( for exmaple: MA, MS, MENG, MED, MSW, MBA)
                    14.    *Professional school degree (for example: MD, DDS, DVM, LLB, JD)
                    15.    *Doctorate degree (for example: PHD, EDD)
                    -1.    *Less than 1st grade
                     0.     Inap. (no spouse/partner;)
                *********************************************************
                    FOR THE PUBLIC DATA SET, CODES 2, 3, 4, 5, 6, AND 7
                    ARE COMBINED WITH CODE 1; CODE 10 AND CODE 11 ARE
                    COMBINED WITH CODE 9, AND; CODES 13, 14, AND 15 ARE
                    COMBINED WITH CODE 12
                *********************************************************


*/

/*Risk and financial literacy - started in 2016*/
if X7556 = -1 then knowledgable_finance = 0;
	else knowledgable_finance = X7556; 


if X7557 = -1 then risk_scale = 0;
	else risk_scale = X7557; 

/*Do you shop around for saving and investment opportunities?*/
if X7562 = -1 then shop_investments_scale = 0;
	else shop_investments_scale = X7562; 



/*Saving for major medical expenses for self or spouse*/
	if X3011 = 3 or X3012 = 3 or X3013 = 3 or X7512 = 3 or X7511 = 3 then save_health = 1;
		else save_health = 0;


/*Health*/
if X7380 = 1 then smokes_self = 1;
	else smokes_self = 0;

if X7395 = 1 then smokes_spouse = 1; 
	else smokes_spouse = 0;



if X6030 = 1 then health_excellent_self = 1;
	else health_excellent_self = 0;

if X6030 = 2 then health_good_self = 1;
	else health_good_self = 0;

if X6030 = 3 then health_fair_self = 1;
	else health_fair_self = 0;

if X6030 = 4 then health_poor_self = 1;
	else health_poor_self = 0;

if X6030 = 3 or X6030 = 4 then health_poor_fair_self = 1;
	else health_poor_fair_self = 0;

if X6030 = 1 then health_self_range = 1;
	else if X6030 = 2 then health_self_range = 2;
	else if X6030 = 3 then health_self_range = 3;
	else if X6030 = 4 then health_self_range = 4;
	else health_self_range = .;



if X6124 = 1 then health_excellent_spouse = 1;
	else health_excellent_spouse = 0;

if X6124 = 2 then health_good_spouse = 1;
	else health_good_spouse = 0;

if X6124 = 3 then health_fair_spouse = 1;
	else health_fair_spouse = 0;

if X6124 = 4 then health_poor_spouse = 1;
	else health_poor_spouse = 0;

if X6124 = 3 or X6124 = 4 then health_poor_fair_spouse = 1;
	else health_poor_fair_spouse = 0;

if X6124 = 0 then health_spouse_range = 0;
	else if X6124 = 1 then health_spouse_range = 1;
	else if X6124 = 2 then health_spouse_range = 2;
	else if X6124 = 3 then health_spouse_range = 3;
	else if X6124 = 4 then health_spouse_range = 4;
	else health_spouse_range = .;



/*To do: create a measure of JOINT HOUSEHOLD HEALTH....MAYBE SOMETHING LIKE SUM HEALTH_SELF AND HEALTH_SPOUSE*/

/*Risk, saving, and attitudes*/  

/*Have foreseeable major expenses are are actively saving for them (e.g., education, health, major purchase, etc*/
   if X7186 = 1 then save_known_expense = 1;
	else if X7186 = 6 then save_known_expense = 1;
	else if X7186 = 0 then save_known_expense = 1;
	else save_known_expense = 0;


/*are you willing to take substantial or above average risk in your investments*/
	if X3014 = 1 or X3014 = 2 then will_take_risk = 1;
	 else will_take_risk = 0;

/*save regularly by putting money aside each month*/
	if X3020 = 1 then save_regularly = 1;
	  else save_regularly = 0;


/*spending exceeded income in last year*/
	if X7510 = 1 then spending_exceed_income1 = 1;
	  else spending_exceed_income1 = 0;


/*spending exceeded income in last year BUT WITHOUT MAJOR PURCHASES SUCH AS A HOUSE OR CAR*/
	if X7508 = 1 then spending_exceed_income2 = 1;
	  else spending_exceed_income2 = 0;


/*Work Information*/
		RHoursPerWeek=X4110; if RHoursPerWeek gt 85 then RHoursPerWeek = 85; if RHoursPerWeek lt 0 then RHoursPerWeek = 0;
		SHoursPerWeek=X4710; if SHoursPerWeek gt 85 then SHoursPerWeek = 85; if SHoursPerWeek lt 0 then SHoursPerWeek = 0;
		CHoursPerWeek=X4110+X4710; if CHoursPerWeek lt 0 then CHoursPerWeek = 0;
		RWeeksPerYear=X4111; if RWeeksPerYear lt 0 then RWeeksPerYear = 0;
		SWeeksPerYear=X4711; if SWeeksPerYear lt 0 then SWeeksPerYear = 0;
		CWeeksPerYear=X4111+X4711; if CWeeksPerYear lt 0 then CWeeksPerYear = 0;

		TRWorkStatus=X6670;
			if TRWorkStatus=1 then RWorkStatus="Employed      "; 
			if TRWorkStatus=2 then RWorkStatus="Temp Laid Off"; 
			if TRWorkStatus=3 then RWorkStatus="Unemployed"; 
			if TRWorkStatus=4 then RWorkStatus="Student"; 
			if TRWorkStatus=5 then RWorkStatus="Homemaker"; 
			if TRWorkStatus=6 then RWorkStatus="Disabled"; 
			if TRWorkStatus=7 then RWorkStatus="Retired"; 
			if TRWorkStatus=8 then RWorkStatus="On Sick Leave"; 
			if TRWorkStatus=10 then RWorkStatus="Volunteer"; 
			if TRWorkStatus=16 then RWorkStatus="Other"; 
			if TRWorkStatus=-7 then RWorkStatus="Other"; 
			if TRWorkStatus=0 then RWorkStatus="Other"; 	
		TSWorkStatus=X6678;
			if TSWorkStatus=1 then SWorkStatus="Employed       "; 
			if TSWorkStatus=2 then SWorkStatus="Temp Laid Off"; 
			if TSWorkStatus=3 then SWorkStatus="Unemployed"; 
			if TSWorkStatus=4 then SWorkStatus="Student"; 
			if TSWorkStatus=5 then SWorkStatus="Homemaker"; 
			if TSWorkStatus=6 then SWorkStatus="Disabled"; 
			if TSWorkStatus=7 then SWorkStatus="Retired"; 
			if TSWorkStatus=8 then SWorkStatus="On Sick Leave"; 
			if TSWorkStatus=10 then SWorkStatus="Volunteer"; 
			if TSWorkStatus=16 then SWorkStatus="Other"; 
			if TSWorkStatus=-7 then SWorkStatus="Other"; 
			if TSWorkStatus=0 then SWorkStatus="No Spouse"; 


		if X6678=1 and X6670=1 then BothEmployed=1; else BothEmployed=0;

		if X6678=5 then SHomemaker=1; else SHomemaker=0;
		if X6670=5 then RHomemaker=1; else RHomemaker=0;

		if X6678=10 then SVolunteer=1; else SVolunteer=0;
		if X6670=10 then RVolunteer=1; else RVolunteer=0;

		if X6670=7 then RRetired=1; else RRetired=0;
		if X6678=7 then SRetired=1; else SRetired=0;


		if X4106=2 then RSelfEmployed = 1; else RSelfEmployed = 0;
		if X4706=2 then SSelfEmployed = 1; else SSelfEmployed = 0;

		if X7093 gt 0 then Rvalue_fambus = X7093; else Rvalue_fambus = 0;
			LABEL Rvalue_fambus = "R's value of businesses in which respondent/spouse have active management role";

		if X7097 gt 0 then Svalue_fambus = X7093; else Svalue_fambus = 0;
			LABEL Svalue_fambus = "Spouse's value of businesses in which respondent/spouse have active management role";



/*****************WORK DETAILED STATUS FOR RESPONDENT AND SPOUSE************************/
/*Create work status variables: mutually exclusive categories plus one multi-category variable*/

/*RESPONDENT Work for others*/
if X4106 = 1 then RWork_others = 1;
	else RWork_others = 0; 


/*Work for self*/
if X4106 = 2 or X4106 = 3 or X4106 = 4 then RWork_self = 1;
	else RWork_self = 0;										/*includes all self employment parnerships in which R has an interest, consultants*/

	/*Subcategories of work for self*/
	if X4106 = 2 then RWork_self_own = 1;						/*only self-owned businesses*/
		else RWork_self_own = 0;

	if X4106 = 3 then RWork_self_partn = 1;						/*only partnerships, law firms, medical, dental*/
		else RWork_self_partn = 0;

	if X4106 = 4 then RWork_self_cons = 1;						/*consultants, contractors*/
		else RWork_self_cons = 0;



/*RESPONDENT Retired*/
if X6670 = 7 or X4100 = 50 then RWork_retire = 1;				/*includes fully retired people*/
	else RWork_retire = 0;										

if RWork_retire = 1 and RWork_others = 1 then RWork_others = 0; 	/*change one case that is retired but also works - the person is retired*/
if RWork_retire = 1 and RWork_self = 1 and RHoursPerWeek ge 35 then RWork_retire = 0;
if RWork_retire = 1 and RWork_self = 1 and RHoursPerWeek lt 35 then RWork_self = 0;


/*RESPONDENT Homemaker*/
if X6670 = 5 or X4100 = 80 then RWork_home_full = 1;
	else RWork_home_full = 0;									/*includes people who identify as homemakers*/


/*define full and part time homemakers by hours worked*/
if RWork_home_full = 1 and RWork_others = 1 and RHoursPerWeek ge 35 then RWork_home_full = 0;
if RWork_home_full = 1 and RWork_others = 1 and RHoursPerWeek lt 10 then RWork_others = 0;

if RWork_home_full = 1 and RWork_others = 1 and (RHoursPerWeek ge 10 and RHoursPerWeek lt 35) then RWork_home_part = 1;
else RWork_home_part = 0;
if RWork_home_part = 1 then RWork_home_full = 0;
if RWork_home_part = 1 then RWork_others = 0; 

if RWork_home_full = 1 and RWork_self = 1 and RHoursPerWeek ge 35 then RWork_home_full = 0;
if RWork_home_full = 1 and RWork_self = 1 and RHoursPerWeek lt 10 then RWork_self = 0;
if RWork_home_full = 1 and RWork_self = 1 and (RHoursPerWeek ge 10 and RHoursPerWeek lt 35) then RWork_home_part = 1;
if RWork_home_part = 1 then RWork_home_full = 0;
if RWork_home_part = 1 then RWork_self = 0; 

if RWork_retire = 1 and RWork_home_full = 1 then RWork_home_full = 0;
if RWork_retire = 1 and RWork_home_part = 1 then RWork_home_part = 0;

if RWork_home_full = 1 or RWork_home_part = 1 then RWork_home = 1;
	else RWork_home = 0;



/*RESPONDENT Unemployed*/
if (X6670 ge 2 and X6670 le 3) or (X6670 = 4) or (X6670 ge 8) or (X4100 = 30) or (X4100 = 85) then RWork_unemploy = 1;
	else if X4100 = -7 or X4100 = 21 or X4100 = 70 then RWork_unemploy = 1;
	else RWork_unemploy = 0;									/*includes laid off, student, sick leave, volunteer, leave of absence, strike, other*/
					
if RWork_others = 1 and RWork_unemploy = 1 and (X6670 = 2 or X6670 = 8) /*change some cases that are temporarily unemployed or on sick leave*/
	then RWork_unemploy = 0; 	

if RWork_others = 1 and RWork_unemploy = 1 and (X6670 = 3 or X6670 = 4 or X6670 = 10 or X6670 = 16) /*change some cases that are unemployed */
	then RWork_others = 0; 	

if RWork_unemploy = 1 and RWork_self = 1 and RHoursPerWeek ge 35 then RWork_unemploy = 0;
if RWork_unemploy = 1 and RWork_self = 1 and RHoursPerWeek lt 35 then RWork_self = 0;
if RWork_unemploy = 1 and RWork_retire = 1 then RWork_unemploy = 0;

if RWork_unemploy = 1 and RWork_home_full = 1 then RWork_home_full = 0;
if RWork_unemploy = 1 and RWork_home_part = 1 then RWork_home_part = 0;
if RWork_unemploy = 1 and RWork_home = 1 then RWork_home = 0;




/*RESPONDENT Disabled*/
if X6670 = 6 or X4100 = 52 then RWork_disabled = 1;				/*includes only people who are disabled and not working*/
	else RWork_disabled = 0;

if RWork_others = 1 and RWork_disabled = 1 then RWork_disabled = 0; 	/*change some cases that are disable but still work*/
if RWork_disabled = 1 and RWork_home_part = 1 then RWork_disabled = 0; 
if RWork_disabled = 1 and RWork_home_full = 1 then RWork_disabled = 0;  
if RWork_disabled = 1 and RWork_home = 1 then RWork_disabled = 0;  


if RWork_disabled = 1 and RWork_self = 1 then RWork_disabled = 0; 
if RWork_disabled = 1 and RWork_retire = 1 then RWork_disabled = 0;

if RWork_disabled = 1 and RWork_unemploy = 1 then RWork_disabled = 0; 


/*RESPONDENT Create multi-category work status variable*/

if RWork_others = 1 then RWork_multi = 1;
	else if RWork_self = 1 then RWork_multi = 2;
	else if RWork_retire = 1 then RWork_multi = 3; 
	else if RWork_unemploy = 1 then RWork_multi = 4;
	else if RWork_home = 1 then RWork_multi = 5;
	else if RWork_disabled = 1 then RWork_multi = 6;
	else RWork_multi = 0;



/*SPOUSE Work for others*/
if X4706 = 1 then SWork_others = 1;
	else SWork_others = 0; 


/*SPOUSE Work for self*/
if X4706 = 2 or X4706 = 3 or X4706 = 4 then SWork_self = 1;
	else SWork_self = 0;										/*includes parnerships in which R has an interest, consultants*/

	/*Subcategories of work for self*/
	if X4706 = 2 then SWork_self_own = 1;
		else SWork_self_own = 0;

	if X4706 = 3 then SWork_self_partn = 1;
		else SWork_self_partn = 0;

	if X4706 = 4 then SWork_self_cons = 1;
		else SWork_self_cons = 0;



/*SPOUSE Retired*/
if X6678 = 7 or X4700 = 50 then SWork_retire = 1;				/*includes fully retired people*/
	else SWork_retire = 0;										

if SWork_retire = 1 and SWork_others = 1 then SWork_others = 0; 	/*change one case that is retired but also works - the person is retired*/
if SWork_retire = 1 and SWork_self = 1 and RHoursPerWeek ge 35 then SWork_retire = 0;
if SWork_retire = 1 and SWork_self = 1 and RHoursPerWeek lt 35 then SWork_self = 0;



/*SPOUSE Homemaker*/
if X6678 = 5 or X4700 = 80 then SWork_home_full = 1;
	else SWork_home_full = 0;									/*includes people who identify as homemakers*/
																/*define full and parttime homemakers by hours worked*/

if SWork_home_full = 1 and SWork_others = 1 and RHoursPerWeek ge 35 then SWork_home_full = 0;
if SWork_home_full = 1 and SWork_others = 1 and RHoursPerWeek lt 10 then SWork_others = 0;

if SWork_home_full = 1 and SWork_others = 1 and (RHoursPerWeek ge 10 and RHoursPerWeek lt 35) then SWork_home_part = 1;
else SWork_home_part = 0;
if SWork_home_part = 1 then SWork_home_full = 0;
if SWork_home_part = 1 then SWork_others = 0; 

if SWork_home_full = 1 and SWork_self = 1 and RHoursPerWeek ge 35 then SWork_home_full = 0;
if SWork_home_full = 1 and SWork_self = 1 and RHoursPerWeek lt 10 then SWork_self = 0;
if SWork_home_full = 1 and SWork_self = 1 and (RHoursPerWeek ge 10 and RHoursPerWeek lt 35) then SWork_home_part = 1;
if SWork_home_part = 1 then SWork_home_full = 0;
if SWork_home_part = 1 then SWork_self = 0; 

if SWork_retire = 1 and SWork_home_full = 1 then SWork_home_full = 0;
if SWork_retire = 1 and SWork_home_part = 1 then SWork_home_part = 0;

if SWork_home_full = . and X4700 = 15 then SWork_home_full = 1;	/*deal with remaining missings from 1989 and 1992*/

if SWork_home_full = 1 or SWork_home_part = 1 then SWork_home = 1;
	else SWork_home = 0;


/*SPOUSE Unemployed*/
if (X6678 ge 2 and X6678 le 3) or (X6678 = 4) or (X6678 ge 8) or (X4700 = 30) or (X4700 = 85) then SWork_unemploy = 1;
	else if X4700 = -7 or X4700 = 21 or X4700 = 70 or X4700 = 90 or X4700 = 0 or X4700 = 199 then SWork_unemploy = 1;
	else SWork_unemploy = 0;									/*includes laid off, student, sick leave, volunteer, leave of absence, strike, other*/
					
if SWork_others = 1 and SWork_unemploy = 1 and (X6678 = 2 or X6678 = 8) /*change some cases that are temporarily unemployed or on sick leave*/
	then SWork_unemploy = 0; 	

if SWork_others = 1 and SWork_unemploy = 1 and (X6678 = 3 or X6678 = 4 or X6678 = 10 or X6678 = 16) /*change some cases that are unemployed */
	then SWork_others = 0; 	

if SWork_unemploy = 1 and SWork_self = 1 and RHoursPerWeek ge 35 then SWork_unemploy = 0;
if SWork_unemploy = 1 and SWork_self = 1 and RHoursPerWeek lt 35 then SWork_self = 0;
if SWork_unemploy = 1 and SWork_retire = 1 then SWork_unemploy = 0;

if SWork_unemploy = 1 and SWork_home_full = 1 then SWork_home_full = 0;
if SWork_unemploy = 1 and SWork_home_part = 1 then SWork_home_part = 0;
if SWork_unemploy = 1 and SWork_home = 1 then SWork_home = 0;



/*SPOUSE Disabled*/
if X6678 = 6 or X4700 = 52 then SWork_disabled = 1;				/*includes only people who are disabled and not working*/
	else SWork_disabled = 0;

if SWork_others = 1 and SWork_disabled = 1 then SWork_disabled = 0; 	/*change some cases that are disable but still work*/
if SWork_disabled = 1 and SWork_home_part = 1 then SWork_disabled = 0; 
if SWork_disabled = 1 and SWork_home_full = 1 then SWork_disabled = 0; 
if SWork_disabled = 1 and SWork_home = 1 then SWork_disabled = 0;  
 
if SWork_disabled = 1 and SWork_self = 1 then SWork_disabled = 0; 
if SWork_disabled = 1 and SWork_retire = 1 then SWork_disabled = 0;

if SWork_disabled = 1 and SWork_unemploy = 1 then SWork_disabled = 0; 



/*SPOUSE Create multi-category work status variable*/

if SWork_others = 1 then SWork_multi = 1;
	else if SWork_self = 1 then SWork_multi = 2;
	else if SWork_retire = 1 then SWork_multi = 3; 
	else if SWork_unemploy = 1 then SWork_multi = 4;
	else if SWork_home = 1 then SWork_multi = 5;
	else if SWork_disabled = 1 then SWork_multi = 6;
	else SWork_multi = 0;
if married2 = 0 then SWork_multi = 0;				/*fix a small number of cases that are coded incorrectly*/




/*****************************************************/
/***RESPONDENT AND SPOUSE INCOME***/

/*inflate to 2019 dollars*/
Rincome = X4112; if Rincome lt 0 then Rincome = 0;
if year=2016 then Rincome   =   Rincome*255.66/240.00;
if year=2013 then Rincome	=	Rincome*255.66/232.96;
if year=2010 then Rincome	=	Rincome*255.66/218.06;
if year=2007 then Rincome	=	Rincome*255.66/207.34;
if year=2004 then Rincome	=	Rincome*255.66/188.90;
if year=2001 then Rincome	=	Rincome*255.66/177.10;
if year=1998 then Rincome	=	Rincome*255.66/163.00;
if year=1995 then Rincome	=	Rincome*255.66/152.40;
if year=1992 then Rincome	=	Rincome*255.66/140.30;
if year=1989 then Rincome	=	Rincome*255.66/124.00;

Sincome = X4712; if Sincome lt 0 then Sincome = 0;
if year=2016 then Sincome	=	Sincome*255.66/240.00;
if year=2013 then Sincome	=	Sincome*255.66/232.96;
if year=2010 then Sincome	=	Sincome*255.66/218.06;
if year=2007 then Sincome	=	Sincome*255.66/207.34;
if year=2004 then Sincome	=	Sincome*255.66/188.90;
if year=2001 then Sincome	=	Sincome*255.66/177.10;
if year=1998 then Sincome	=	Sincome*255.66/163.00;
if year=1995 then Sincome	=	Sincome*255.66/152.40;
if year=1992 then Sincome	=	Sincome*255.66/140.30;
if year=1989 then Sincome	=	Sincome*255.66/124.00;

Rbusincome = X4131; if Rbusincome lt 0 then Rbusincome = 0;
if year=2016 then Rbusincome	=	Rbusincome*255.66/240.00;
if year=2013 then Rbusincome	=	Rbusincome*255.66/232.96;
if year=2010 then Rbusincome	=	Rbusincome*255.66/218.06;
if year=2007 then Rbusincome	=	Rbusincome*255.66/207.34;
if year=2004 then Rbusincome	=	Rbusincome*255.66/188.90;
if year=2001 then Rbusincome	=	Rbusincome*255.66/177.10;
if year=1998 then Rbusincome	=	Rbusincome*255.66/163.00;
if year=1995 then Rbusincome	=	Rbusincome*255.66/152.40;
if year=1992 then Rbusincome	=	Rbusincome*255.66/140.30;
if year=1989 then Rbusincome	=	Rbusincome*255.66/124.00;

Sbusincome = X4731; if Sbusincome lt 0 then Sbusincome = 0;
if year=2016 then Sbusincome	=	Sbusincome*255.66/240.00;
if year=2013 then Sbusincome	=	Sbusincome*255.66/232.96;
if year=2010 then Sbusincome	=	Sbusincome*255.66/218.06;
if year=2007 then Sbusincome	=	Sbusincome*255.66/207.34;
if year=2004 then Sbusincome	=	Sbusincome*255.66/188.90;
if year=2001 then Sbusincome	=	Sbusincome*255.66/177.10;
if year=1998 then Sbusincome	=	Sbusincome*255.66/163.00;
if year=1995 then Sbusincome	=	Sbusincome*255.66/152.40;
if year=1992 then Sbusincome	=	Sbusincome*255.66/140.30;
if year=1989 then Sbusincome	=	Sbusincome*255.66/124.00;



/*****************************************************/
/***HEALTH INSURANCE***/


/*Does ANYONE in the household have health insurance?*/
if X6341 = 1 then HInsure_any = 1; 
	else HInsure_any = 0;


/*Does EVERYONE in the household have health insurance?*/
if X6357 = 1 then HInsure_all = 1;
	else HInsure_all = 0;


/*Who is NOT covered by health insurance?*/
if X6358 = 1 then HInsure_no_resp	= 1;
	else  HInsure_no_resp = 0;

if X6359 = 1 then HInsure_no_spouse	= 1;
	else  HInsure_no_spouse = 0;

if X6360 = 1 or X6361 then HInsure_no_kids	= 1;	/*kids under 18 and kids over 18*/
	else  HInsure_no_kids = 0;

if X6362 = 1 then HInsure_no_parents	= 1;
	else  HInsure_no_parents = 0;


/*Who pays for health insurance*/
if X6351 = 1 then HInsure_self_pay = 1;
	else HInsure_self_pay = 0;

if X6352 = 1 or X6354 = 1 then HInsure_work_pay = 1;	/*current or former employer pays for insurance*/
	else HInsure_work_pay = 0;

if X6353 = 1 then HInsure_relative_pay = 1;
	else HInsure_relative_pay = 0;

if X6355 = 1 then HInsure_govt_pay = 1;
	else HInsure_govt_pay = 0;




/*****************************************************/
/***GENERAL GIFTS AND INHERITANCE -- SEE BELOW FOR DETAILS ON TRANSFERRED REAL ESTATE AND BUSINESSES***/
		/*EVER RECEIVED*/
		if X5801=1 then ReceivedInheritance=1;
			else ReceivedInheritance=0;

		/*NUMBER OF INHERITANCES*/
		NumberInheritances	=	X5802;

		/*Types:
		0. did not receive an inheritance
		1. inheritance; life insurance; other settlement; inherited trust
		2. trust
		3. transfer/gift
		4. other 
		I changed values of -7 to 4; according to the SCF codebook:
                NOTE: the large number of -7 codes for this variable reflect
                a programming error that cause the verbatim response to the
                question to be skipped.

		I changed values of 6 (inherited trust) to 2. the codebook says to call these inheritance, but Wolff calls them trusts.

		I changed values of 5 to 1; there were only 5 responses of this sort to inheritance type 2; 
			the codebook makes no mention of this code, so I put them in with the most common type.
		*/		

/**I create a variable below that indicates whether the respondent received *any* of each of the three categories**/


		/*TYPE OF INHERITANCE -- FIRST INHERITANCE*/
		InheritanceType1	= 	X5803;
			if InheritanceType1 = -7
			then InheritanceType1 = 4; 

			else if InheritanceType1 = 6
			then InheritanceType1 = 2; 

			If InheritanceType1 = 1
			then InheritanceType1_inherit = 1;
			else InheritanceType1_inherit = 0;

			If InheritanceType1 = 3
			then InheritanceType1_gift = 1;
			else InheritanceType1_gift = 0;

			If InheritanceType1 = 2 or InheritanceType1 = 4
			then InheritanceType1_trust = 1;
			else InheritanceType1_trust = 0;

		/*TYPE OF INHERITANCE -- SECOND INHERITANCE*/
		InheritanceType2	= 	X5808;
			if InheritanceType2 = -7
			then InheritanceType2 = 4; 

			else if InheritanceType2 = 6
			then InheritanceType2 = 2; 

			else if InheritanceType2 = 5
			then InheritanceType2 = 1; 

			If InheritanceType2 = 1
			then InheritanceType2_inherit = 1;
			else InheritanceType2_inherit = 0;

			If InheritanceType2 = 3
			then InheritanceType2_gift = 1;
			else InheritanceType2_gift = 0;

			If InheritanceType2 = 2 or InheritanceType1 = 4
			then InheritanceType2_trust = 1;
			else InheritanceType2_trust = 0;

		/*TYPE OF INHERITANCE -- THIRD INHERITANCE*/
		InheritanceType3	= 	X5813;
			if InheritanceType3 = -7
			then InheritanceType3 = 4; 

			else if InheritanceType3 = 6
			then InheritanceType3 = 2; 

			If InheritanceType3 = 1
			then InheritanceType3_inherit = 1;
			else InheritanceType3_inherit = 0;

			If InheritanceType3 = 3
			then InheritanceType3_gift = 1;
			else InheritanceType3_gift = 0;

			If InheritanceType3 = 2 or InheritanceType1 = 4
			then InheritanceType3_trust = 1;
			else InheritanceType3_trust = 0;

		
		/*INHERITANCE VALUE for three separate gifts; code inheritance less than $500 as no inheritance*/
		InheritanceValue1	= 	X5804;
			if InheritanceValue1 = -1
			then InheritanceValue1 = 0;
			else if InheritanceValue1 lt 500
			then InheritanceValue1 = 0;

		InheritanceValue2	= 	X5809;
			if InheritanceValue2 = -1
			then InheritanceValue2 = 0;
			else if InheritanceValue2 lt 500
			then InheritanceValue2 = 0;

		InheritanceValue3	= 	X5814;
			if InheritanceValue3 = -1
			then InheritanceValue3 = 0;
			else if InheritanceValue3 lt 500
			then InheritanceValue3 = 0;


		/*I changed -1 values to 0; -1 indicates an inheritance with no market value;
			there were very few cases, so this doesn't make a huge difference;
			moreover, a value-less inheritance is unlikely to be consequential.*/


		YearReceived1		=	X5805;
		YearReceived2		=	X5810;
		YearReceived3		=	X5815;


		TimeSinceReceived1	=	Year - YearReceived1;
			If YearReceived1 = 0 then TimeSinceReceived1 = 0;

		TimeSinceReceived2	=	Year - YearReceived2;
			If YearReceived2 = 0 then TimeSinceReceived2 = 0;

		TimeSinceReceived3	=	Year - YearReceived3;
			If YearReceived3 = 0 then TimeSinceReceived3 = 0;


		if  YearReceived1 = 2010 or YearReceived1 = 2013 or 
			YearReceived2 = 2010 or YearReceived2 = 2013 or
			YearReceived3 = 2010 or YearReceived3 = 2013 
			then YearReceived_2010_2013 = 1;
			else YearReceived_2010_2013 = 0;

		if  (YearReceived1 le 2009 and YearReceived1 ge 2000) or 
			(YearReceived2 le 2009 and YearReceived2 ge 2000) or
			(YearReceived3 le 2009 and YearReceived3 ge 2000) 
			then YearReceived_2000s = 1;
			else YearReceived_2000s = 0;


		/*SOURCE*/

		InheritFrom1	=	X5806;
			if InheritFrom1 = 2 then InheritFrom1_parents = 1;
			else InheritFrom1_parents = 0;

			if InheritFrom1 = 1 then InheritFrom1_grandparents = 1;
			else InheritFrom1_grandparents = 0;

			if InheritFrom1 = 3 or InheritFrom1 = 4 or InheritFrom1 = 5 or InheritFrom1 = 7 or 
				InheritFrom1 = 11 or InheritFrom1 = 25 or InheritFrom1 = 30 then InheritFrom1_relative = 1;
			else InheritFrom1_relative = 0;

			if InheritFrom1 = 6 or InheritFrom1 = 12 or InheritFrom1 = -7 then InheritFrom1_other = 1;
			else InheritFrom1_other = 0;


		InheritFrom2	=	X5811;
			if InheritFrom2 = 2 then InheritFrom2_parents = 1;
			else InheritFrom2_parents = 0;

			if InheritFrom2 = 1 then InheritFrom2_grandparents = 1;
			else InheritFrom2_grandparents = 0;

			if InheritFrom2 = 3 or InheritFrom2 = 4 or InheritFrom2 = 5 or InheritFrom2 = 7 or 
				InheritFrom2 = 11 or InheritFrom2 = 25 or InheritFrom2 = 30 then InheritFrom2_relative = 1;
			else InheritFrom2_relative = 0;

			if InheritFrom2 = 6 or InheritFrom2 = 12 or InheritFrom2 = -7 then InheritFrom2_other = 1;
			else InheritFrom2_other = 0;


		InheritFrom3	=	X5816;
			if InheritFrom3 = 2 then InheritFrom3_parents = 1;
			else InheritFrom3_parents = 0;

			if InheritFrom3 = 1 then InheritFrom3_grandparents = 1;
			else InheritFrom3_grandparents = 0;

			if InheritFrom3 = 3 or InheritFrom3 = 4 or InheritFrom3 = 5 or InheritFrom3 = 7 or 
				InheritFrom3 = 11 or InheritFrom3 = 25 or InheritFrom3 = 30 then InheritFrom3_relative = 1;
			else InheritFrom3_relative = 0;

			if InheritFrom3 = 6 or InheritFrom3 = 12 or InheritFrom3 = -7 then InheritFrom3_other = 1;
			else InheritFrom3_other = 0;

		/*Create summary variable - any inheritance by source*/

			if InheritFrom1_parents = 1 or InheritFrom2_parents = 1 or InheritFrom3_parents = 1 then InheritFrom_parents = 1;
			else InheritFrom_parents = 0;

			if InheritFrom1_grandparents = 1 or InheritFrom2_grandparents = 1 or InheritFrom3_grandparents = 1 then InheritFrom_grandparents = 1;
			else InheritFrom_grandparents = 0;

			if InheritFrom1_relative = 1 or InheritFrom2_relative = 1 or InheritFrom3_relative = 1 then InheritFrom_relative = 1;
			else InheritFrom_relative = 0;

			if InheritFrom1_other = 1 or InheritFrom2_other = 1 or InheritFrom3_other = 1 then InheritFrom_other = 1;
			else InheritFrom_other = 0;


		/*From the codebook:   codes 7 and 11 appear in data but not in codebook. i coded them as other
					 1.    *Grandparent
                     2.    *Parent (include current or former parents-in-law)
                     3.    *Child
                     4.    *Aunt/Uncle
                     5.    *Sibling
                     6.    *Friend
                    12.     Government settlement; compensation
                    25.     Family, n.e.c.
                    30.     Divorced former spouse
                    -7.    *Other
                     0.     Inap. (no inheritances: X5801^=1; no inheritances:
                            X5802<1/less than 2 inheritances: X5802<2/
                            less than 3 inheritances: X5802<3)

					Per an email with the SCF staff, in 1989 and 1992, there were also codes of:
					7		Cousin
					11		Deceased spouse
					I include these as other relatives

                *********************************************************
                    FOR THE PUBLIC DATA SET, CODE 30 IS COMBINED WITH
                    CODE 25
                *********************************************************
*/


		/*HOW MUCH ALTOGETHER WERE ANY OTHER INHERITANCES YOU RECEIVED?*/
		OtherInheritance	=	X5818;
			if OtherInheritance = -1
			then OtherInheritance = 0;

		/*DO YOU EXPECT TO RECEIVE ANY ADDITIONAL INHERITANCES? IF SO, HOW MUCH?*/
		AmountExpected		=	X5821;

		if AmountExpected 	lt 1000 then AmountExpected = 0;

		if AmountExpected =0 then ExpectInheritance = 0;
			else ExpectInheritance = 1;


		/*inflate to 2019 dollars*/
		if year=2016 then AmountExpected	=	AmountExpected*255.66/240.00;
		if year=2013 then AmountExpected	=	AmountExpected*255.66/232.96;
		if year=2010 then AmountExpected	=	AmountExpected*255.66/218.06;
		if year=2007 then AmountExpected	=	AmountExpected*255.66/207.34;
		if year=2004 then AmountExpected	=	AmountExpected*255.66/188.90;
		if year=2001 then AmountExpected	=	AmountExpected*255.66/177.10;
		if year=1998 then AmountExpected	=	AmountExpected*255.66/163.00;
		if year=1995 then AmountExpected	=	AmountExpected*255.66/152.40;
		if year=1992 then AmountExpected	=	AmountExpected*255.66/140.30;
		if year=1989 then AmountExpected	=	AmountExpected*255.66/124.00;


		TotalInheritance	=	InheritanceValue1 + InheritanceValue2 + InheritanceValue3 + OtherInheritance;

		if year=2016 then TotalInheritance	=	TotalInheritance*255.66/240.00;
		if year=2013 then TotalInheritance	=	TotalInheritance*255.66/232.96;
		if year=2010 then TotalInheritance	=	TotalInheritance*255.66/218.06;
		if year=2007 then TotalInheritance	=	TotalInheritance*255.66/207.34;
		if year=2004 then TotalInheritance	=	TotalInheritance*255.66/188.90;
		if year=2001 then TotalInheritance	=	TotalInheritance*255.66/177.10;
		if year=1998 then TotalInheritance	=	TotalInheritance*255.66/163.00;
		if year=1995 then TotalInheritance	=	TotalInheritance*255.66/152.40;
		if year=1992 then TotalInheritance	=	TotalInheritance*255.66/140.30;
		if year=1989 then TotalInheritance	=	TotalInheritance*255.66/124.00;

	

		logexpect			= log(AmountExpected+1);
		loginherit			= log(TotalInheritance+1);


		/*recode any total inheritance of 0 as no inheritance;
		  this recodes those with any inheritance of less than $500 as having no inheritance*/

		If TotalInheritance = 0 then ReceivedInheritance = 0;


		/*create variables indicating receipt of inheritance types, summing across the three transfers*/

		If InheritanceType1_inherit = 1 or InheritanceType2_inherit = 1 or InheritanceType3_inherit = 1
		then ReceivedInheritance_inherit = 1;
		else ReceivedInheritance_inherit = 0;

		If InheritanceType1_gift = 1 or InheritanceType2_gift = 1 or InheritanceType3_gift = 1
		then ReceivedInheritance_gift = 1;
		else ReceivedInheritance_gift = 0;

		If InheritanceType1_trust = 1 or InheritanceType2_trust = 1 or InheritanceType3_trust = 1
		then ReceivedInheritance_trust = 1;
		else ReceivedInheritance_trust = 0;


expect_amount = ExpectInheritance*Logexpect;


if AmountExpected ge 100000000 then Expect_100million = 1;
	else Expect_100million = 0;
	LABEL Expect_100million = "Expect to inherit 100 million plus; this is an extra variable"; 


if AmountExpected ge 50000000 then Expect_50million = 1;
	else Expect_50million = 0;
	LABEL Expect_50million = "Expect to inherit 50 million plus"; 

if AmountExpected ge 1000000 and AmountExpected lt 50000000 then Expect_million = 1;
	else Expect_million = 0;
	LABEL Expect_million = "Expect to inherit 1-49 million"; 

if AmountExpected ge 700000 and AmountExpected lt 1000000 then Expect_700k = 1;
	else Expect_700k = 0;
		LABEL Expect_700k = "Expect to inherit 700,000 to 999,999"; 

if AmountExpected ge 250000 and AmountExpected lt 700000 then Expect_250k = 1;
	else Expect_250k = 0;		
		LABEL Expect_250k = "Expect to inherit 250,000 to 699,999"; 

if AmountExpected ge 75000 and AmountExpected lt 250000 then Expect_75k = 1;
	else Expect_75k = 0;		
		LABEL Expect_75k = "Expect to inherit 75,000 to 249,999"; 

if AmountExpected ge 10000 and AmountExpected lt 75000 then Expect_10k = 1;
	else Expect_10k = 0;
		LABEL Expect_10k = "Expect to inherit 10,000 to 74,999"; 

if AmountExpected ge 1000 and AmountExpected lt 10000 then Expect_1k = 1;
	else Expect_1k = 0;
		LABEL Expect_1k = "Expect to inherit 1,000 to 9,999"; 


/*create a single variable for amount expected to inherit*/
	if Expect_50million 	= 1 then Expect_categories = 7;
	else if Expect_million 	= 1 then Expect_categories = 6; 
	else if Expect_700k 	= 1 then Expect_categories = 5;
	else if Expect_250k 	= 1 then Expect_categories = 4;
	else if Expect_75k	 	= 1 then Expect_categories = 3;
	else if Expect_10k	 	= 1 then Expect_categories = 2;
	else if Expect_1k	 	= 1 then Expect_categories = 1;
	else Expect_categories = 0;




/*****************************************************/
/*GIFTS AND INHERITANCES GIVEN AS REAL ESTATE AND BUSINESS....
		per SCF codebook, these are included in general transfers; thus these are details on how transfers occurred*/

		if X608 = 1  or X618 = 1  or X628 = 1  or X632 = 1 or X636 = 1 or X718 = 1 then InheritResRealEstate = 1;
		   else InheritResRealEstate = 0;

		if X1710 = 1 or X1810 = 1 or X2004 = 1 or X2014 = 1 then InheritOtherRealEstate = 1;
		   else InheritOtherRealEstate = 0;

		if X3108 = 3 or X3108 = 4 or X3108 = 10 or X3208 = 3 or X3208 = 4 or X3208 = 10 then InheritBusiness = 1;
			else InheritBusiness = 0;

		if InheritResRealEstate = 1 or InheritOtherRealEstate = 1 or InheritBusiness = 1 then InheritRealEstateOrBusiness = 1;
			else InheritRealEstateOrBusiness = 0; 



/*Year is coded as character in some years and numeric in other years; this code standardizes that*/
		yeartemp=year*1;
			drop year;
			rename yeartemp=year;


/*Characters to numbers for ID and year
		X1=Y1;
		XX1=YY1;
		Y1=X1*1;
		YY1=XX1*1;
		*/

/*###################################################################*/
/*##################  Brent's child age and baby variables   ########*/
/*##################  follow                     ####################*/
/*###################################################################*/
if X110>0 	then P3_Age=X110; 	if X110=. 					then P3_Age=.;
if X110<0	& X110^=. then P3_baby=1; 	else P3_baby=0; 
if X116>0 	then P4_Age=X116; 	if X116=. 					then P4_Age=.;
if X116<0	& X116^=. then P4_baby=1; 	else P4_baby=0; 
if X122>0 	then P5_Age=X122; 	if X122=. 					then P5_Age=.;
if X122<0	& X122^=. then P5_baby=1; 	else P5_baby=0; 
if X128>0 	then P6_Age=X128; 	if X128=. 					then P6_Age=.;
if X128<0	& X128^=. then P6_baby=1; 	else P6_baby=0; 
if X134>0 	then P7_Age=X134; 	if X134=. 					then P7_Age=.;
if X134<0	& X134^=. then P7_baby=1; 	else P7_baby=0; 
if X204>0 	then P8_Age=X204; 	if X204=. 					then P8_Age=.;
if X204<0	& X204^=. then P8_baby=1; 	else P8_baby=0;
if X210>0 	then P9_Age=X210; 	if X210=. 					then P9_Age=.;
if X210<0	& X210^=. then P9_baby=1; 	else P9_baby=0;
if X216>0 	then P10_Age=X216; 	if X216=. 					then P10_Age=.; 
if X216<0	& X216^=. then P10_baby=1; 	else P10_baby=0; 
if X222>0 	then P11_Age=X222; 	if X222=. 					then P11_Age=.;
if X222<0	& X222^=. then P11_baby=1; 	else P11_baby=0; 
if X228>0 	then P12_Age=X228; 	if X228=. 					then P12_Age=.;
if X228<0	& X228^=. then P12_baby=1; 	else P12_baby=0; 

has_toddler=0;
if P3_age <6 & P3_age^=. then has_toddler=1;
if P4_age <6 & P4_age^=. then has_toddler=1;
if P5_age <6 & P5_age^=. then has_toddler=1;
if P6_age <6 & P6_age^=. then has_toddler=1;
if P7_age <6 & P7_age^=. then has_toddler=1;
if P8_age <6 & P8_age^=. then has_toddler=1;
if P9_age <6 & P9_age^=. then has_toddler=1;
if P10_age <6 & P10_age^=. then has_toddler=1;
if P11_age <6 & P11_age^=. then has_toddler=1;
if P12_age <6 & P12_age^=. then has_toddler=1;

has_kids=0;
if P3_age<18 & P3_age^=. 	then has_kids=1;
if P4_age<18 & P4_age^=. 	then has_kids=1;
if P5_age<18 & P5_age^=. 	then has_kids=1;
if P6_age<18 & P6_age^=. 	then has_kids=1;
if P7_age<18 & P7_age^=. 	then has_kids=1;
if P8_age<18 & P8_age^=. 	then has_kids=1;
if P9_age<18 & P9_age^=. 	then has_kids=1;
if P10_age<18 & P10_age^=. 	then has_kids=1;
if P11_age<18 & P11_age^=. 	then has_kids=1;
if P12_age<18 & P12_age^=. 	then has_kids=1;

N_kids=0;
if P3_age<18 & P3_age^=. then N_kids+1;
if P4_age<18 & P4_age^=. then N_kids+1;
if P5_age<18 & P5_age^=. then N_kids+1;
if P6_age<18 & P6_age^=. then N_kids+1;
if P7_age<18 & P7_age^=. then N_kids+1;
if P8_age<18 & P8_age^=. then N_kids+1;
if P9_age<18 & P9_age^=. then N_kids+1;
if P10_age<18 & P10_age^=. then N_kids+1;
if P11_age<18 & P11_age^=. then N_kids+1;
if P12_age<18 & P12_age^=. then N_kids+1;

/*abbreviated variable names*/
Nkid=N_kids;
has_t=has_toddler;


/*Drop all variables from the full data set; these take up an incredible amount of space*/
drop J14-J33001 X14-X42000; 
run;



/*Sort Each dataset prior to merging*/
	proc sort data=temp1;
		by p_id replicate year;
	run;
	proc sort data=temp2;
		by p_id replicate year;
	run;

/*Merge Full Info into Extract info*/
	Data temp3;
		merge temp1 temp2;
		by p_id replicate year;

/*****************************************************/
/*Create a few more variables using information from the merged data*/

/*Inheritance/NW ratio*/
InheritanceNWRatio=TotalInheritance/(Networth+.01);


/*Received an inheritance before age 30*/

	Age30Year = birthyear + 30;

	if  (YearReceived1 gt 0 and YearReceived1 le Age30Year) or 
		(YearReceived2 gt 0 and YearReceived2 le Age30Year) or 
		(YearReceived3 gt 0 and YearReceived3 le Age30Year) 
	 		then Received_Before30 = 1;
			else Received_Before30 = 0;

	if ReceivedInheritance = 1 and Received_Before30 = 0
			then Received_After30 = 1;
			else Received_After30 = 0;

/*Received an inheritance before age 31*/

	Age31Year = birthyear + 31;

	if  (YearReceived1 gt 0 and YearReceived1 le Age31Year) or 
		(YearReceived2 gt 0 and YearReceived2 le Age31Year) or 
		(YearReceived3 gt 0 and YearReceived3 le Age31Year) 
	 		then Received_Before31 = 1;
			else Received_Before31 = 0;

	if ReceivedInheritance = 1 and Received_Before31 = 0
			then Received_After31 = 1;
			else Received_After31 = 0;


/*Received an inheritance ages 32-48*/

	Age32Year = birthyear + 32;
	Age48Year = birthyear + 48;

	if  (YearReceived1 gt 0 and YearReceived1 ge Age32Year and YearReceived1 le Age48Year) or 
		(YearReceived2 gt 0 and YearReceived2 ge Age32Year and YearReceived2 le Age48Year) or 
		(YearReceived3 gt 0 and YearReceived3 ge Age32Year and YearReceived3 le Age48Year) 
	 		then Received_Ages32_48 = 1;
			else Received_Ages32_48 = 0;


/*Received an inheritance ages 49-67*/

	Age49Year = birthyear + 49;
	Age67Year = birthyear + 67;

	if  (YearReceived1 gt 0 and YearReceived1 ge Age49Year and YearReceived1 le Age67Year) or 
		(YearReceived2 gt 0 and YearReceived2 ge Age49Year and YearReceived2 le Age67Year) or 
		(YearReceived3 gt 0 and YearReceived3 ge Age49Year and YearReceived3 le Age67Year) 
	 		then Received_Ages49_67 = 1;
			else Received_Ages49_67 = 0;


/*Received an inheritance ages 68-95*/

	Age68Year = birthyear + 68;
	Age95Year = birthyear + 95;

	if  (YearReceived1 gt 0 and YearReceived1 ge Age68Year and YearReceived1 le Age95Year) or 
		(YearReceived2 gt 0 and YearReceived2 ge Age68Year and YearReceived2 le Age95Year) or 
		(YearReceived3 gt 0 and YearReceived3 ge Age68Year and YearReceived3 le Age95Year) 
	 		then Received_Ages68_95 = 1;
			else Received_Ages68_95 = 0;



/*Assets greater than total inheritance received*/
AssetsGreaterThanInheritance = 0;
If Assets gt TotalInheritance then AssetsGreaterThanInheritance = 1;
	if TotalInheritance = 0 then AssetsGreaterThanInheritance = 0; 


/*Create largest inheritance and time since receiving that transfer*/

	/***IF YOU USE THESE, INFLATE THEM!!!!!*/

If ReceivedInheritance = 0 then LargestInheritance = 0;
	else LargestInheritance		= max(InheritanceValue1,InheritanceValue2,InheritanceValue3); 

If ReceivedInheritance = 0 then LoggedLargestInheritance = 0;
	else LogTotInh = log(LargestInheritance + 1);

If ReceivedInheritance = 0 then TimeSinceLargest = 0;
	else if LargestInheritance 	= InheritanceValue1 then TimeSinceLargest = TimeSinceReceived1;
	else if LargestInheritance 	= InheritanceValue2 then TimeSinceLargest = TimeSinceReceived2;
	else if LargestInheritance 	= InheritanceValue3 then TimeSinceLargest = TimeSinceReceived3;

If ReceivedInheritance = 0 then YearReceivedLargest = 0;
	else if LargestInheritance 	= InheritanceValue1 then YearReceivedLargest = YearReceived1;
	else if LargestInheritance 	= InheritanceValue2 then YearReceivedLargest = YearReceived2;
	else if LargestInheritance 	= InheritanceValue3 then YearReceivedLargest = YearReceived3;


/*Age at Which Received Largest Inheritance
	-Set to 0 for those who did not receive an inheritance
	-Set to 1 for those who received an inheritance before they were born
	-Set to birthyear + 25 to correct an SCF data mistake*/
If ReceivedInheritance = 0 												 then AgeReceivedLargest = 0;
	else if ReceivedInheritance = 1 									 then AgeReceivedLargest = YearReceivedLargest - birthyear;

If AgeReceivedLargest lt 0 then AgeReceivedLargest = 1; 
If YearReceivedLargest = 0 and LargestInheritance gt 0 then YearReceivedLargest = birthyear + 25;


time_value=TimeSinceLargest*loginherit;


/*Create interactions between time since received and education level*/
		Time_LessThanHS 		= TimeSinceLargest*LessThanHS;
		Time_HSGraduate			= TimeSinceLargest*HSGraduate;
		Time_SomeCollege		= TimeSinceLargest*SomeCollege;
		Time_Bachelors			= TimeSinceLargest*Bachelors;
		Time_AdvancedDegree		= TimeSinceLargest*AdvancedDegree;

		
		TimeSince_LessThanHS1 		= TimeSinceReceived1*LessThanHS;
		TimeSince_HSGraduate1		= TimeSinceReceived1*HSGraduate;
		TimeSince_SomeCollege1		= TimeSinceReceived1*SomeCollege;
		TimeSince_Bachelors1		= TimeSinceReceived1*Bachelors;
		TimeSince_AdvancedDegree1	= TimeSinceReceived1*AdvancedDegree;

		TimeSince_LessThanHS2 		= TimeSinceReceived2*LessThanHS;
		TimeSince_HSGraduate2		= TimeSinceReceived2*HSGraduate;
		TimeSince_SomeCollege2		= TimeSinceReceived2*SomeCollege;
		TimeSince_Bachelors2		= TimeSinceReceived2*Bachelors;
		TimeSince_AdvancedDegree2	= TimeSinceReceived2*AdvancedDegree;

		TimeSince_LessThanHS3 		= TimeSinceReceived3*LessThanHS;
		TimeSince_HSGraduate3		= TimeSinceReceived3*HSGraduate;
		TimeSince_SomeCollege3		= TimeSinceReceived3*SomeCollege;
		TimeSince_Bachelors3		= TimeSinceReceived3*Bachelors;
		TimeSince_AdvancedDegree3	= TimeSinceReceived3*AdvancedDegree;


/*Create interactions between time since received and cohort*/
		TimeSinceY	= TimeSinceLargest*generation_Y;
		TimeSinceX	= TimeSinceLargest*generation_X;
		TimeSincebb	= TimeSinceLargest*generation_bb;
		TimeSincegg	= TimeSinceLargest*generation_gg;

		TimeSince_generation_Y1 		= TimeSinceReceived1*generation_Y;
		TimeSince_generation_X1			= TimeSinceReceived1*generation_X;
		TimeSince_generation_bb1		= TimeSinceReceived1*generation_bb;
		TimeSince_generation_gg1		= TimeSinceReceived1*generation_gg;

		TimeSince_generation_Y2 		= TimeSinceReceived2*generation_Y;
		TimeSince_generation_X2			= TimeSinceReceived2*generation_X;
		TimeSince_generation_bb2		= TimeSinceReceived2*generation_bb;
		TimeSince_generation_gg2		= TimeSinceReceived2*generation_gg;

		TimeSince_generation_Y3 		= TimeSinceReceived3*generation_Y;
		TimeSince_generation_X3			= TimeSinceReceived3*generation_X;
		TimeSince_generation_bb3		= TimeSinceReceived3*generation_bb;
		TimeSince_generation_gg3		= TimeSinceReceived3*generation_gg;


		TimeSinceLargest_parents		= TimeSinceLargest*InheritFrom_parents;
		TimeSinceLargest_grandparents	= TimeSinceLargest*InheritFrom_grandparents;
		TimeSinceLargest_relative		= TimeSinceLargest*InheritFrom_relative;
		TimeSinceLargest_other			= TimeSinceLargest*InheritFrom_other;


/*Create interactions between inheriting anything and cohort*/
		genY_inherit = generation_Y*receivedinheritance;
		genX_inherit = generation_X*receivedinheritance;
		genbb_inherit = generation_bb*receivedinheritance;
		gengg_inherit = generation_gg*receivedinheritance;

		genbb_old_inherit = generation_bb_old*receivedinheritance;
		genbb_yng_inherit = generation_bb_yng*receivedinheritance;



		if generation_Y = 1 and receivedinheritance = 0 then genY_noin = 1; else genY_noin = 0;
		if generation_X = 1 and receivedinheritance = 0 then genX_noin = 1; else genX_noin = 0;
		if generation_bb = 1 and receivedinheritance = 0 then genbb_noin = 1; else genbb_noin = 0;
		if generation_gg = 1 and receivedinheritance = 0 then gengg_noin = 1; else gengg_noin = 0;



/*Create interactions between inherited amount and cohort*/
		genY_amount = generation_Y*loginherit;
		genX_amount = generation_X*loginherit;
		genbb_amount = generation_bb*loginherit;
		gengg_amount = generation_gg*loginherit;

		genbb_old_amount = generation_bb_old*loginherit;
		genbb_yng_amount = generation_bb_yng*loginherit;

 
/*Create interactions between generation and age received*/
		AgeinhY		= AgeReceivedLargest*generation_Y;
		AgeinhX		= AgeReceivedLargest*generation_X;
		Ageinhbb	= AgeReceivedLargest*generation_bb;
		Ageinhgg	= AgeReceivedLargest*generation_gg;


		AgeAmtY		= AgeReceivedLargest*generation_Y*TotalInheritance;
		AgeAmtX		= AgeReceivedLargest*generation_X*TotalInheritance;
		AgeAmtbb	= AgeReceivedLargest*generation_bb*TotalInheritance;
		AgeAmtgg	= AgeReceivedLargest*generation_gg*TotalInheritance;


/*Create year indicators*/

Yearsq = year*year; 

if year ge 1989 and year le 1998 then year90s = 1; 		else year90s 	= 0;
if year ge 2001 and year le 2004 then year01_04 = 1; 	else year01_04 	= 0;
if year ge 2007 and year le 2010 then year07_10 = 1; 	else year07_10 	= 0;

if year = 1989 then year89 = 1; else year89 = 0;
if year = 1992 then year92 = 1; else year92 = 0;
if year = 1995 then year95 = 1; else year95 = 0;
if year = 1998 then year98 = 1; else year98 = 0;
if year = 2001 then year01 = 1; else year01 = 0;
if year = 2004 then year04 = 1; else year04 = 0; 
if year = 2007 then year07 = 1; else year07 = 0;
if year = 2010 then year10 = 1; else year10 = 0;
if year = 2013 then year13 = 1; else year13 = 0;
if year = 2016 then year16 = 1; else year16 = 0;
if year = 2019 then year19 = 1; else year19 = 0;


wealth_income	= loggednetworth*loggedincome;

year_logwealth 	= year*loggednetworth;
year_logincome	= year*loggedincome;


black_logwealth		= black*loggednetworth;
white_logwealth 	= white*loggednetworth;
latino_logwealth 	= latino*loggednetworth;
other_logwealth 	= other*loggednetworth;

black_logincome		= black*loggedincome;
white_logincome 	= white*loggedincome;
latino_logincome 	= latino*loggedincome;
other_logincome 	= other*loggedincome;


year90s_black	= year90s*black;
year90s_white	= year90s*white;
year90s_latino	= year90s*latino;
year90s_other	= year90s*other;

year01_04_black	= year01_04*black;
year01_04_white	= year01_04*white;
year01_04_latino= year01_04*latino;
year01_04_other	= year01_04*other;

year07_10_black	= year07_10*black;
year07_10_white	= year07_10*white;
year07_10_latino= year07_10*latino;
year07_10_other	= year07_10*other;

year13_black	= year13*black;
year13_white	= year13*white;
year13_latino	= year13*latino;
year13_other	= year13*other;



/*Create Measure of TOP HOUSEHOLDS 1%, 90%, 80%, 50% by Year
	-P99 is a proc means option; Networth99 is a new data set; Networth99v is the new variable*/

/*******TOP NET WORTH*******/
Proc Means data=temp3 P99 P90 P80 P50 noprint;
		Var NetWorth;
		by year;
		Weight wgt; 
		output 	out = Networth99 P99=Networth99v P90=Networth90v P80=Networth80v P50=Networth50v;
	run;

/*Produce net worth thresholds for 1%, 80%, 90%, and 50% by year*/
proc means data = Networth99; var Networth99v Networth90v Networth80v Networth50v; by year; run;

DATA Temp3;
		merge Temp3 Networth99;
		by year;
		run;


/*******TOP FINANCIAL ASSET OWNERS*******/
Proc Means data=temp3 P99 P90 P80 P50 noprint;
		Var Financial;
		by year;
		Weight wgt; 
		output 	out = Fin99 P99=Fin99v P90=Fin90v P80=Fin80v P50=Fin50v;
	run;

/*Produce financial asset thresholds for 1%, 80%, 90%, and 50% by year*/
proc means data = Fin99; var Fin99v Fin90v Fin80v Fin50v; by year; run;

DATA Temp3;
		merge Temp3 Fin99;
		by year;
		run;


/*******TOP INHERITORS*******/
Proc Means data=Temp3 P99 P90 P80 P50 noprint;
		Var TotalInheritance;
		by year;
		Weight wgt; 
		output 	out = Inherit99 P99=Inherit99v P90=Inherit90v P80=Inherit80v P50=Inherit50v;
	run;

/*Produce inheritance thresholds for 1%, 80%, 90%, and 50% by year*/
proc means data = Inherit99; var Inherit99v Inherit90v Inherit80v Inherit50v; by year; run;


DATA Temp3;
		merge Temp3 Inherit99;
		by year;
		run;


/*******TOP INCOME EARNERS*******/
Proc Means data=Temp3 P99 P90 P80 P50 noprint;
		Var Income;
		by year;
		Weight wgt; 
		output 	out = Income99 P99=Income99v P90=Income90v P80=Income80v P50=Income50v;
	run;

/*Produce income thresholds for 1%, 80%, 90%, and 50% by year*/
proc means data = Income99; var Income99v Income90v Income80v Income50v; by year; run;

DATA Temp3;
		merge Temp3 Income99;
		by year;
		run;


DATA Temp3;
	set Temp3;


/*Create top net worth measures*/

if Networth ge Networth99v then Percent1 = 1; 
	else if Networth lt Networth99v then Percent1 = 0;
	else if Networth = . then Percent1 = .;

if Networth ge Networth90v and Networth lt Networth99v then Percent9 = 1;
	else if Networth lt Networth90v then Percent9 = 0;
	else if Networth ge Networth99v then Percent9 = 0;
	else if Networth = . then Percent9 = .;

if Networth ge Networth80v and Networth lt Networth90v then Percent80 = 1;
	else if Networth lt Networth80v then Percent80 = 0;
	else if Networth ge Networth90v then Percent80 = 0;
	else if Networth = . then Percent80 = .;

if Networth lt Networth80v then PercentOth = 1;
	else if Networth ge Networth80v then PercentOth = 0;
	else if Networth = . then PercentOth = .;

/*Create single mutually exclusive net worth variable*/
If Percent1 = 1 then NWPerc = 1;
	if Percent9 = 1 then NWPerc = 2;
	if Percent80 = 1 then NWPerc = 3;
	if PercentOth = 1 then NWPerc = 4;
	if Networth = . then NWPerc = .;


/*Create Proportion Owned by Top One Percent USING TWO DIFFERENT STRATEGIES*/

/*STRATEGY 1: use Proc tabulate to get sums; 
	this produces a table in the output file that includes sums; 
	use that table to produce % owned in Excel*/
proc tabulate data=Temp3;
class year NWPerc;
var Networth;
table year *(NWPerc all), Networth * (N mean median sum);
weight wgt;
run;


/*STRATEGY 2: do means by year and net worth category...keeping all the different class parts, which
   gives us the totals we'll need to divide by */
proc means data=Temp3 noprint;
class year NWPerc;
var Networth;
output out=NWTot sum=NWSum;
weight wgt;
run;

/* merge it onto itself, selecting out the year total (type 2) and 
   the category total (type 3), then divide to get proportion 
TO SEE RESULTS:
1. go to explorer tab (left)
2. open work directory
3. open file called Nwprop - column on farthest right is proportion owned by each of the mutually exclusive categories 1-4 defined above*/
data NWProp;
 merge NWTot (where=(_type_=2) rename=(NWSum=YearTot)) 
       NWTot (where=(_type_=3));
 by year;
 propbyDist=NWSum/yeartot;
run;


DATA Temp3;
	set Temp3;

/*Create top financial asset owner measures*/

if Financial ge Fin99v then Fin1 = 1; 
	else if Financial lt Fin99v then Fin1 = 0;
	else if Financial = . then Fin1 = .;

if Financial ge Fin90v and Financial lt Fin99v then Fin9 = 1;
	else if Financial lt Fin90v then Fin9 = 0;
	else if Financial ge Fin99v then Fin9 = 0;
	else if Financial = . then Fin9 = .;

if Financial ge Fin80v and Financial lt Fin90v then Fin80 = 1;
	else if Financial lt Fin80v then Fin80 = 0;
	else if Financial ge Fin90v then Fin80 = 0;
	else if Financial = . then Fin80 = .;

if Financial lt Fin80v then FinOth = 1;
	else if Financial ge Fin80v then FinOth = 0;
	else if Financial = . then FinOth = .;

/*Create single mutually exclusive net worth variable*/
If Fin1 = 1 then FinPerc = 1;
	if Fin9 = 1 then FinPerc = 2;
	if Fin80 = 1 then FinPerc = 3;
	if FinOth = 1 then FinPerc = 4;
	if Financial = . then FinPerc = .;


/*Create Proportion Owned by Top One Percent USING TWO DIFFERENT STRATEGIES*/

/*STRATEGY 1: use Proc tabulate to get sums; 
	this produces a table in the output file that includes sums; 
	use that table to produce % owned in Excel*/
proc tabulate data=Temp3;
class year FinPerc;
var Financial;
table year *(FinPerc all), Financial * (N mean median sum);
weight wgt;
run;


/*STRATEGY 2: do means by year and net worth category...keeping all the different class parts, which
   gives us the totals we'll need to divide by */
proc means data=Temp3 noprint;
class year FinPerc;
var Financial;
output out=FinTot sum=FinSum;
weight wgt;
run;

/* merge it onto itself, selecting out the year total (type 2) and 
   the category total (type 3), then divide to get proportion 
TO SEE RESULTS:
1. go to explorer tab (left)
2. open work directory
3. open file called Nwprop - column on farthest right is proportion owned by each of the mutually exclusive categories 1-4 defined above*/
data FinProp;
 merge FinTot (where=(_type_=2) rename=(FinSum=YearTot)) 
       FinTot (where=(_type_=3));
 by year;
 propbyDist=FinSum/yeartot;
run;


DATA Temp3;
	set Temp3;

/*Create top inheritor measures*/

if TotalInheritance ge 1000000 then Inherit_Million = 1;
	else if TotalInheritance lt 1000000 then Inherit_Million = 0;
	else if TotalInheritance = . then Inherit_Million = .;

if TotalInheritance ge 50000 then Inherit_FiftyK = 1;
	else if TotalInheritance lt 50000 then Inherit_FiftyK = 0;
	else if TotalInheritance = . then Inherit_FiftyK = .;


if TotalInheritance ge Inherit99v then InheritPercent1 = 1; 
	else if TotalInheritance lt Inherit99v then InheritPercent1 = 0;
	else if TotalInheritance = . then InheritPercent1 = .;

if TotalInheritance ge Inherit90v and TotalInheritance lt Inherit99v then InheritPercent9 = 1;
	else if TotalInheritance lt Inherit90v then InheritPercent9 = 0;
	else if TotalInheritance ge Inherit99v then InheritPercent9 = 0;
	else if TotalInheritance = . then InheritPercent9 = .;

if TotalInheritance ge Inherit80v and TotalInheritance lt Inherit90v then InheritPercent80 = 1;
	else if TotalInheritance lt Inherit80v then InheritPercent80 = 0;
	else if TotalInheritance ge Inherit90v then InheritPercent80 = 0;
	else if TotalInheritance = . then InheritPercent80 = .;

if TotalInheritance lt Inherit80v then InheritPercentOth = 1;
	else if TotalInheritance ge Inherit80v then InheritPercentOth = 0;
	else if TotalInheritance = . then InheritPercentOth = .;

/*Create single mutually exclusive inheritor variable*/
If InheritPercent1 = 1 then INHPerc = 1;
	if InheritPercent9 = 1 then INHPerc = 2;
	if InheritPercent80 = 1 then INHPerc = 3;
	if InheritPercentOth = 1 then INHPerc = 4;
	if TotalInheritance = . then INHPerc = .;


/*Create Proportion of all inheritance received by Top One Percent USING TWO DIFFERENT STRATEGIES*/

/*STRATEGY 1: use Proc tabulate to get sums; 
	this produces a table in the output file that includes sums; 
	use that table to produce % owned in Excel*/
proc tabulate data=Temp3;
class year INHPerc;
var TotalInheritance;
table year *(INHPerc all), TotalInheritance * (N mean median sum);
weight wgt;
run;


/*STRATEGY 2: do means by year and inheritance category...keeping all the different class parts, which
   gives us the totals we'll need to divide by */
proc means data=Temp3 noprint;
class year INHPerc;
var TotalInheritance;
output out=INHTot sum=INHSum;
weight wgt;
run;

/* merge it onto itself, selecting out the year total (type 2) and 
   the category total (type 3), then divide to get proportion 
TO SEE RESULTS:
1. go to explorer tab (left)
2. open libraries/work directory
3. open file called Inhwprop - column on farthest right is proportion owned by each of the mutually exclusive categories 1-4 defined above*/
data INHProp;
 merge INHTot (where=(_type_=2) rename=(INHSum=YearTot2)) 
       INHTot (where=(_type_=3));
 by year;
 propbyDist2=INHSum/yeartot2;
run;




DATA Temp3;
	set Temp3;

/*Create top income measures*/

if Income ge Income99v then IncomePercent1 = 1; 
	else if Income lt Income99v then IncomePercent1 = 0;
	else if Income = . then IncomePercent1 = .;

if Income ge Income90v and Income lt Income99v then IncomePercent9 = 1;
	else if Income lt Income90v then IncomePercent9 = 0;
	else if Income ge Income99v then IncomePercent9 = 0;
	else if Income = . then IncomePercent9 = .;

if Income ge Income80v and Income lt Income90v then IncomePercent80 = 1;
	else if Income lt Income80v then IncomePercent80 = 0;
	else if Income ge Income90v then IncomePercent80 = 0;
	else if Income = . then IncomePercent80 = .;

if Income lt Income80v then IncomePercentOth = 1;
	else if Income ge Income80v then IncomePercentOth = 0;
	else if Income = . then IncomePercentOth = .;

/*Create single mutually exclusive income variable*/
If IncomePercent1 = 1 then INCPerc = 1;
	if IncomePercent9 = 1 then INCPerc = 2;
	if IncomePercent80 = 1 then INCPerc = 3;
	if IncomePercentOth = 1 then INCPerc = 4;
	if Income = . then INCPerc = .;


/*Create Proportion of all income received by Top One Percent USING TWO DIFFERENT STRATEGIES*/

/*STRATEGY 1: use Proc tabulate to get sums; 
	this produces a table in the output file that includes sums; 
	use that table to produce % owned in Excel*/
proc tabulate data=Temp3;
class year INCPerc;
var Income;
table year *(INCPerc all), Income * (N mean median sum);
weight wgt;
run;


/*STRATEGY 2: do means by year and INCOME category...keeping all the different class parts, which
   gives us the totals we'll need to divide by */
proc means data=Temp3 noprint;
class year INCPerc;
var Income;
output out=INCTot sum=INCSum;
weight wgt;
run;

/* merge it onto itself, selecting out the year total (type 2) and 
   the category total (type 3), then divide to get proportion 
TO SEE RESULTS:
1. go to explorer tab (left)
2. open libraries/work directory
3. open file called incprop - column on farthest right is proportion owned by each of the mutually exclusive categories 1-4 defined above*/
data INCProp;
 merge INCTot (where=(_type_=2) rename=(INCSum=YearTot3)) 
       INCTot (where=(_type_=3));
 by year;
 propbyDist3=INCSum/yeartot3;
run;



/*STRATEGY 2 VARIATION 2: do means by year and inheritance category...keeping all the different class parts, which
   gives us the totals we'll need to divide by */
proc means data=Temp3 noprint;
class year IncomePercent1;
var Income;
output out=INCTot2 sum=INCSum2;
weight wgt;
run;

/* merge it onto itself, selecting out the year total (type 2) and 
   the category total (type 3), then divide to get proportion 
TO SEE RESULTS:
1. go to explorer tab (left)
2. open libraries/work directory
3. open file called incprop2 - column on farthest right is proportion owned by the top 1% and the remaining 99%*/
data INCProp2;
 merge INCTot2 (where=(_type_=2) rename=(INCSum2=YearTot4)) 
       INCTot2 (where=(_type_=3));
 by year;
 propbyDist4=INCSum2/yeartot4;
run;



DATA Temp3;
	set Temp3;

/*create measures of joint membership in top income and wealth positions*/
if percent1 = 1 and incomepercent1 = 1 then joint_both = 1;
	else if percent1 = . or incomepercent1 = . then joint_both = .;
	else joint_both = 0;

if percent1 = 1 and incomepercent1 = 0 then joint_wealth_only = 1;
	else if percent1 = . or incomepercent1 = . then joint_wealth_only = .;
	else joint_wealth_only = 0;

if percent1 = 0 and incomepercent1 = 1 then joint_income_only = 1;
	else if percent1 = . or incomepercent1 = . then joint_income_only = .;
	else joint_income_only = 0;

if percent1 = 0 and incomepercent1 = 0 then joint_none = 1;
	else if percent1 = . or incomepercent1 = . then joint_none = .;
	else joint_none = 0;




/*Estimate frequency of 1%. With weights 1%=1%*/
	proc freq data = Temp3;
	tables Percent1 IncomePercent1 joint_both joint_wealth_only joint_income_only joint_none;
	weight wgt;
	run;

data temp3;
	set temp3; 


/*Division of Labor variables*/
/*##################################################################################################################################*/
/*Define division of labor types.
When I first did this, we included ages 25-65 in our analyses, and there were 3,496 cases where either 1) both work parttime or 2) one works part time, is not retired, 
	and the other works 0 hours.
of those:
-705 are in the top one percent by wealth; mean wealth=$77million; median wealth=$32million; mean income=$4.9m median income=$1.8m; 
-646 are in the top one percent by income  mean wealth= median wealth= mean income= median income=; 
-576 are in the top by both income and wealth mean wealth= median wealth= mean income= median income=

We ultimately changed our strategy to include those aged 25-60; we also added the part-time workers to the traditional, dual, and progressive groups.

Our groups are:
				Husband hours per week	Wife hours per week		Explanation
non-worker		<10						<10						BOTH less than 10 hours; we omit these couples (see 1.c. above)
traditional		>10						<10						Husband works, wife doesn't; BUT most of these husbands work full time or >35 hours
neo-traditional	>35						>10 but <35				Husband works full time, wife part-time
dual			>35						>35
				or BOTH
				>10						>10						Both work full-time or both work part-time; In most of these couples, both work full time
Neo-progressive	>10 but <35				>35						Wife works full time, husband part time
Progressive		<10						>10						Wife works, husband doesn't; BUT most of these wives work full time or >35 hours


	*/


	/*Non-workers*/
	if RHoursPerWeek lt 10 and SHoursPerWeek lt 10 then DOL_NonWorkers = 1;
		else DOL_NonWorkers = 0;

	/*Traditional: Male breadwinner/female homemaker*/
	if RHoursPerWeek ge 10 then R_work1 = 1; else R_work1 = 0;
	if SHoursPerWeek lt 10 then S_work1 = 1; else S_work1 = 0;
	
	if R_work1 = 1 and S_work1 = 1 then DOL_Trad = 1; else DOL_Trad = 0;

	/*Neo-Traditional: Male breadwinner/female part-time worker*/
	if RHoursPerWeek ge 35 then R_work2 = 1; else R_work2 = 0;
	if SHoursPerWeek ge 10 and SHoursPerWeek le 34 then S_work2 = 1; else S_work2 = 0;
	
	if R_work2 = 1 and S_work2 = 1 then DOL_NEOTrad = 1; else DOL_NEOTrad = 0;

	/*Dual-earners*/
	if RHoursPerWeek ge 35 	then R_work3 = 1; else R_work3 = 0;
	if SHoursPerWeek ge 35  then S_work3 = 1; else S_work3 = 0;
	
	if R_work3 = 1 and S_work3 = 1 then DOL_Dual = 1; 
		else if ((RHoursPerWeek ge 10 and RHoursPerWeek le 34) and (SHoursPerWeek ge 10 and SHoursPerWeek le 34)) then DOL_Dual = 1;
		else DOL_Dual = 0;

	/*Progressive: Female breadwinner/male homemaker*/
	if RHoursPerWeek lt 10 	then R_work4 = 1; else R_work4 = 0;
	if SHoursPerWeek ge 10  then S_work4 = 1; else S_work4 = 0;
	
	if R_work4 = 1 and S_work4 = 1 then DOL_Prog = 1; else DOL_Prog = 0;

	/*Neo-Progressive: Female breadwinner/male part-time worker*/
	if RHoursPerWeek ge 10 and RHoursPerWeek le 34 then R_work5 = 1; else R_work5 = 0;
	if SHoursPerWeek ge 35 then S_work5 = 1; else S_work5 = 0;

	if R_work5 = 1 and S_work5 = 1 then DOL_NEOProg = 1; else DOL_NEOProg = 0;


/*Create multi-category DOL variable
	Missing values on DOL_ThreeCat include non-workers, progressive and neo-progressive couples; I checked thoroughly!*/
	if DOL_Trad 		= 1 then DOL_ThreeCat = 1;
	else if DOL_NEOTrad = 1 then DOL_ThreeCat = 2;
	else if DOL_Dual 	= 1 then DOL_ThreeCat = 3;
	else DOL_ThreeCat = .;

	/*###################################################################
	DOL_NonWorkers
	DOL_Trad
	DOL_NeoTrad
	DOL_Dual
	DOL_Prog
	DOL_NeoProg
	######################################################################*/

/*Create year-specific variables to model trend*/
	if Percent1 = 1 and (year = 1989 or year = 1992 or year = 1995) then Year1_NW = 1;
		else Year1_NW = 0;

	if Percent1 = 1 and (year = 1998 or year = 2001 or year = 2004 or year = 2007) then Year2_NW = 1;
		else Year2_NW = 0;

	if Percent1 = 1 and (year = 2010 or year = 2013 or year = 2016) then Year3_NW = 1;
		else Year3_NW = 0;


	if IncomePercent1 = 1 and (year = 1989 or year = 1992 or year = 1995) then Year1_INC = 1;
		else Year1_INC = 0;

	if IncomePercent1 = 1 and (year = 1998 or year = 2001 or year = 2004 or year = 2007) then Year2_INC = 1;
		else Year2_INC = 0;

	if IncomePercent1 = 1 and (year = 2010 or year = 2013 or year = 2016) then Year3_INC = 1;
		else Year3_INC = 0;


/*Create change in income*/
		Income_change = income - Normal_income; 

		If income = . or Normal_income = . then Change_none = .; 
			else if Income_change = 0 then Change_none = 1; 
			else if Income_change ne 0 then Change_none = 0;

		If income = . or Normal_income = . then Change_increase = .; 
			else if Income_change gt 0 then Change_increase = 1; 
			else if Income_change le 0 then Change_increase = 0;

		If income = . or Normal_income = . then Change_decrease = .; 
			else if Income_change lt 0 then Change_decrease = 1; 
			else if Income_change ge 0 then Change_decrease = 0;



/*The following variables were added by Brent. They are a conversion of 
	the original coding from mutually exclusive BINARY variables to 
	mutually exclusive CATEGORICAL variables. This is because the
	Divison of Labor models need these to be categorical */

kids_fourcat=0;
	if N_Kids=1 then kids_fourcat=1;
	if N_kids=2 then kids_fourcat=2;
	if N_kids>2 then kids_fourcat=3;
	if N_kids=. then kids_fourcat=.;


/*missing values on these variables reflect missing values in the original data; i checked*/
R_ed_fivecat=.;
	if R_LessThanHS=1 then R_ed_fivecat=0;
	if R_HSGraduate=1 then R_ed_fivecat=1;
	if R_SomeCollege=1 then R_ed_fivecat=2;
	if R_Bachelors=1 then R_ed_fivecat=3;
	if R_AdvancedDegree=1 then R_ed_fivecat=4;

SP_ed_fivecat=.;
	if SP_LessTHanHS=1 then SP_ed_fivecat=0;
	if SP_HSGraduate=1 then SP_ed_fivecat=1;
	if SP_SomeCollege=1 then SP_ed_fivecat=2;
	if SP_Bachelors=1 then SP_ed_fivecat=3;
	if SP_AdvancedDegree=1 then SP_ed_fivecat=4;

if year = 1989 | year=1992 | year=1995 | year=1998 then Nineties=1; else Nineties=0;
if year = 2001 | year=2004 | year=2007 then Aughts=2; else Aughts=0;
if year = 2010 | year=2013 | year=2016 then Teens=3; else Teens=0;


/*abbreviated variable (to simplify interaction effect output)*/
K4=kids_fourcat;


/*Save a copy for analysis*/
Data SCF2.SCFforAnalysis2;
	set temp3;
	run;

proc print data=temp3 (obs=1000); 
run;

proc print data=SCF2.SCFforAnalysis2 (obs=1000); 
run;


/*Create means across 5 implicates (i.e., imputed values for each household*/

	proc means data = SCF2.SCFforAnalysis2 noprint nway;
	class p_id; output out = SCF2.SCFforAnalysis_Means2 mean = ; 
	run;




/***************************************************************************************/
/***************************************************************************************/
/***************************************************************************************/
/***************************************************************************************/
/***************************************************************************************/
/***************************************************************************************/

/*KEEP THIS STUFF - IT IS FOR THE DATA ANALYTICS CLASS. WILL UPDATE WITH 2019 DATA ASAP*/


/*Create dataset for Data Analytics class - give them:
	1. all variables in extract files (temp1)
	2. select variables in full data files (temp2)
	3. select variables in merged extract-full (I created others in this merged dataset)  (temp3)
	4. means across imputations
*/

/*full extract files*/
data temp_class1;
	set temp1;

proc sort data = temp_class1;
	by year P_id replicate;
	run;

/*select variables from full data files*/
data temp_class2;
	set temp2 (keep = year replicate p_id TotalInheritance LogInherit Save_Regularly
Expect_50million 	
 Expect_million 
 Expect_700k 	
 Expect_250k 	
 Expect_75k	 	
 Expect_10k	 	
 Expect_1k	 	
Expect_categories
R_occup_FIRE S_occup_FIRE Both_occup_FIRE);
	run;

proc sort data = temp_class2;
	by year P_id replicate;
	run;


/*select variables from full data files*/
data temp_class3;
	set temp3 (keep = year replicate p_id Percent1 IncomePercent1 Joint_Both);
	run;

proc sort data = temp_class3;
	by year P_id replicate;
	run;

 


data temp_class8;
	merge temp_class1 temp_class2 temp_class3;
	by year p_id replicate;
	run;

/*create means and save final data set*/
proc means data = temp_class8 noprint nway;
	class p_id; output out = SCF.SCFforDataClass_Means mean = ; 
	run;




data temp9;
	set scf.SCFforDataClass_Means;

if year lt 2016 then delete;

data scf.SCF2016;
	set temp9;
run;

data scf.SCF2016_small;
	set temp9 (keep = race income num_kids);
run;



